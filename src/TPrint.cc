/**
 * @file TPrint.cc
 * @brief Source code of the TPrint class
 *
 * @details
 * This file contains the implementation of the TPrint class.
 *
 * @date 2019-03-27
 * @author Marco Meyer <marco.meyer@cern.ch>
 */

#include "TPrint.h"
ClassImp(TPrint)

TPrint *gPrint   = new TPrint();

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

int        TPrint::kTabular     = 0;
const char TPrint::kTabularChar = '\t';

int  TPrint::kDebug          = (TString(gSystem->Getenv("DEBUG")).Atoi());
bool TPrint::kQuiet          = (TString(gSystem->Getenv("QUIET")).Atoi() != 0);
bool TPrint::kSuperQuiet     = (TString(gSystem->Getenv("SUPERQUIET")).Atoi() != 0);
bool TPrint::kCarriageReturn = true;

std::vector<int> TPrint::asserts;
int TPrint::iTTY       = 0;
int TPrint::iFailedTTY = 0;

std::map<TString, std::chrono::high_resolution_clock::time_point> TPrint::onceX = {};
std::chrono::time_point<std::chrono::system_clock> TPrint::progressBarTime = std::chrono::system_clock::now();

int TPrint::kStdOut_dup = dup(1);
std::vector<int> TPrint::kStdOut;
std::vector<TString> TPrint::kStdOut_fname;

std::vector<TString> TPrint::kTrash;

int TPrint::kStdErr_dup = dup(2);
std::vector<int> TPrint::kStdErr;
std::vector<TString> TPrint::kStdErr_fname;

std::map<TString, TString> TPrint::envlist;
std::vector<TString> TPrint::envhashmap;
std::vector<double> TPrint::envcoeffmap;

bool TPrint::bSingleSkipIncrementTab = false;
bool TPrint::bSingleCarriageReturn = false;
bool TPrint::bShortRedirectTTY = false;

const bool TPrint::kIgnoreTab = false;
const bool TPrint::kIncrementTab = !TPrint::kIgnoreTab;
const TString TPrint::kUnchangedColor = "";
const TString TPrint::kNoColor = "\033[0m";
const TString TPrint::kRed = "\033[1;31m";
const TString TPrint::kGreen = "\033[1;32m";
const TString TPrint::kOrange = "\033[1;33m";
const TString TPrint::kBlue = "\033[1;34m";
const TString TPrint::kPurple = "\033[1;35m";
const TString TPrint::kOrangeBlink = "\033[33;5m";

TPrint::TPrint() { }
TPrint::~TPrint() { }

bool TPrint::RunTests(const char* title)
{
	TPrint::Tests(title);
	return asserts.size() == count(asserts.begin(), asserts.end(), 1);
}

void TPrint::Tests(const char* title)
{
	int total     = asserts.size();
	int remaining = count(asserts.begin(), asserts.end(),  0);
	int failed    = count(asserts.begin(), asserts.end(), -1);
	int succeeded = count(asserts.begin(), asserts.end(),  1);

	TPrint::Info(title, TPrint::kOrange + "Test Summary: " + TPrint::kNoColor);
	TPrint::DecrementTab();
	TPrint::Info(title, "- There are %d scheduled test(s), %s%d missing assert(s)%s", total, 
		remaining > 1 ? TPrint::kRed.Data() : TPrint::kNoColor.Data(), remaining, TPrint::kNoColor.Data()
	);
	
	TPrint::DecrementTab();
	TPrint::Info(title, "- In total %s%d test(s) got executed%s out of %d, %s%d test(s) succeeded%s and %s%d test(s) failed%s", 
		total-remaining > 1 ? TPrint::kOrange.Data() : TPrint::kNoColor.Data(), total-remaining, TPrint::kNoColor.Data(), 
		total,
		      succeeded > 0 ? TPrint::kGreen.Data()  : TPrint::kNoColor.Data(), succeeded,       TPrint::kNoColor.Data(), 
		         failed > 1 ? TPrint::kRed.Data()    : TPrint::kNoColor.Data(), failed,          TPrint::kNoColor.Data());
	TPrint::DecrementTab();
}

void TPrint::ScheduleTests(int N)
{
	asserts.resize(asserts.size() + N);

	int total     = asserts.size();
	int failed    = count(asserts.begin(), asserts.end(), -1);
	int succeeded = count(asserts.begin(), asserts.end(),  1);

	int remaining = count(asserts.begin(), asserts.end(),  0);
	TString remainingStr = remaining != total ? ", " + TString::Itoa(remaining, 10) + " test(s) remaining" : "";

	TPrint::Debug(10, __METHOD_NAME__, 
		"You scheduled "   + TString::Itoa(N,10)      + 
		" new test(s) => " + TString::Itoa(total, 10) + " test(s) in total" + remainingStr);
}

void TPrint::Assert(const char* title, bool b, int pos)
{
	if(pos < 0) {
	
		pos = find(asserts.begin(), asserts.end(), 0) - asserts.begin();
		if(TPrint::ErrorIf(pos == asserts.size(), __METHOD_NAME__, "Too many tests.. Please schedule more tests."))
			return;
	}

	if(asserts[pos] != 0) {

		if(TPrint::ErrorIf(pos == asserts.size(), __METHOD_NAME__, "Assertion #"+TString::Itoa(pos,10)+" already got executed.."))
			return;
	}

	if(TPrint::ErrorIf(pos > asserts.size() - 1, __METHOD_NAME__, "Unexpected test index received.. Please schedule more tests."))
		return;

	asserts[pos] = b ? 1 : -1;
	TPrint::TestBar(title);

	std::cout << std::flush;
}

TString TPrint::GetTestBar(const char* title)
{
	int total     = asserts.size();
	int succeeded = count(asserts.begin(), asserts.end(),  1);

	double percentage = double(succeeded)/double(total);
	int val = (int) (percentage * 100);
	
	TString str     = "";
	for(int i = 0; i < total; i++) {

		if(asserts[i] > 0) str += "o";
		else if(asserts[i] < 0) str += "x";
		else str += ".";
	}
	
	TString head    = (!TString(title).EqualTo("")) ? TString(title) : "";
	TString message = Form("%3d%% [%s] (%d/%d)", val, str.Data(), succeeded, total);
	if (TPrint::IsQuiet()) return MakeItShorter(head + message);

	TString os;
	os += TPrint::kPurple + MakeItShorter(head) + TPrint::kNoColor;
	os += ": Test(s)..  ";
	os += MakeItShorter(message);

	return os;
}


void TPrint::TestBar(const char* title)
{
	bool bHoldSingleCarriageReturn = bSingleCarriageReturn;
	TString str = TPrint::GetTestBar(title);
	bSingleCarriageReturn = bHoldSingleCarriageReturn;
	if(!str.EqualTo("")) {

		std::cout << TPrint::CarriageReturn(1);
		std::cout << TPrint::Tab();
		std::cout << str;
		std::cout << TPrint::CarriageReturn();
		std::cout << std::flush;
	}

	int remaining = count(asserts.begin(), asserts.end(),  0);
	if(remaining < 1) {

		if(bSingleCarriageReturn) std::cout << TPrint::CarriageReturn(1);
		else std::cout << std::endl;

		if(TPrint::IsQuiet() && !TPrint::IsSuperQuiet()) std::cout << std::endl;
	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

void TPrint::ResetTab() {
	TPrint::kTabular = 0;
}
void TPrint::IncrementTab() {
	TPrint::kTabular++;
}

void TPrint::DecrementTab()
{
	if(TPrint::kTabular > 0) TPrint::kTabular--;
}

void TPrint::ProgressBar(const char* title, Option_t *option, int event, int total, int refresh)
{
	TString opt = option;
			opt.ToLower();

	if(event != total || !opt.Contains("noclean"))
		TPrint::SetSingleCarriageReturn();
	
	bool bHoldSingleCarriageReturn = bSingleCarriageReturn;
	TString str = TPrint::GetProgressBar(title, event, total, refresh);
	
	bSingleCarriageReturn = bHoldSingleCarriageReturn;
	if(!str.EqualTo("")) {

		std::cout << TPrint::CarriageReturn(1);
		std::cout << TPrint::Tab();
		std::cout << str;
		std::cout << TPrint::CarriageReturn();
		std::cout << std::flush;
	}

	if(event == total) {

		if(bSingleCarriageReturn) {

			std::cout << TPrint::CarriageReturn(1);
			ClearLine();
		
		} else std::cout << std::endl;

		if(TPrint::IsQuiet() && !TPrint::IsSuperQuiet()) std::cout << std::endl;
	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

bool TPrint::OnceEvery(int ms, const char *file, int line)
{
	TString key = TString(file) + ":" + TString::Itoa(line,10) + ":" + TString::Itoa(ms,10);
    auto currentTime = std::chrono::high_resolution_clock::now();
    if (onceX.find(key) == onceX.end() || currentTime - onceX[key] >= std::chrono::milliseconds(ms)) {
        onceX[key] = currentTime;
		return true;
    }

	return false;
}

void TPrint::PrintMessageEveryXSeconds(int x) {

    static std::chrono::high_resolution_clock::time_point lastPrintTime;

    auto currentTime = std::chrono::high_resolution_clock::now();
    if (currentTime - lastPrintTime >= std::chrono::seconds(x)) {
        std::cout << "Message printed at " << x << " seconds interval." << std::endl;
        lastPrintTime = currentTime;
    }
}

void TPrint::Debug(int iDebugLevel, const char* title, TVector3 &v3, bool spherical_coordinate)
{
	if(!spherical_coordinate) {

		std::vector<double> v = {v3.Px(), v3.Py(), v3.Pz()};
		TPrint::Debug(iDebugLevel, title, v);

	} else {

		std::vector<double> v2 = {v3.Mag(), v3.Theta()/TMath::Pi()*180, v3.Phi()/TMath::Pi()*180};
		TPrint::Debug(iDebugLevel, title, v2);
	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

void TPrint::Debug(int iDebugLevel, const char* title, TLorentzVector &lv, bool spherical_coordinate)
{
	if(!spherical_coordinate) {

		std::vector<double> v = {lv.Px(), lv.Py(), lv.Pz(), lv.E()};
		TPrint::Debug(iDebugLevel, title, v);

	} else {

		std::vector<double> v2 = {lv.Rho(), lv.Theta()/TMath::Pi()*180, lv.Phi()/TMath::Pi()*180, lv.E()};
		TPrint::Debug(iDebugLevel, title, v2);
	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

TString TPrint::GetDate(const std::chrono::time_point<std::chrono::system_clock>& tp, const TString& format)
{
    std::time_t time_t = std::chrono::system_clock::to_time_t(tp);
    std::tm local_tm = *std::localtime(&time_t);

    std::stringstream ss;
    ss << std::put_time(&local_tm, format.Data());
    return TString(ss.str().c_str());
}

TString TPrint::GetDate(long long timestamp, const TString& format)
{
	std::time_t unix_timestamp = timestamp;
	char time_buf[80];
	struct tm ts;
	ts = *localtime(&unix_timestamp);
	strftime(time_buf, sizeof(time_buf), format.Data(), &ts);

	return TString(time_buf);
}

TString TPrint::GetTime(long long timestamp, const TString& format)
{
	std::time_t unix_timestamp = timestamp;
	char time_buf[80];
	struct tm ts;
	ts = *gmtime(&unix_timestamp);
	strftime(time_buf, sizeof(time_buf), format.Data(), &ts);

	TString time = TString(time_buf);
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, '0');
			time = (TString) time.Strip(TString::kLeading, 'h');
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, '0');
			time = (TString) time.Strip(TString::kLeading, 'm');
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, 's');
			time = (TString) time.Strip(TString::kBoth, ' ');

	return time;
}

TString TPrint::GetTime(const std::chrono::time_point<std::chrono::system_clock>& tp, const TString& format)
{
    std::time_t time_t = std::chrono::system_clock::to_time_t(tp);
    std::tm gmt_tm = *std::gmtime(&time_t);

    char time_buf[80];
    std::strftime(time_buf, sizeof(time_buf), format.Data(), &gmt_tm);

	TString time = TString(time_buf);
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, '0');
			time = (TString) time.Strip(TString::kLeading, 'h');
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, '0');
			time = (TString) time.Strip(TString::kLeading, 'm');
			time = (TString) time.Strip(TString::kBoth, ' ');
			time = (TString) time.Strip(TString::kLeading, 's');
			time = (TString) time.Strip(TString::kBoth, ' ');

	return time;
}

TString TPrint::GetProgressBar(const char* title, int event, int total, int refresh)
{
	TString progressBarStr = "";
	if(event == 0) progressBarTime = std::chrono::system_clock::now();
	else 
	{
		std::chrono::duration<double> elapsedTime = std::chrono::system_clock::now() - progressBarTime;
		uint64_t time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		progressBarStr = Form(", dt = %.2fs, %s - %s ETA ~ %s", 
			elapsedTime.count()/event, 
			GetDate(progressBarTime).Data(),
			GetDate(time + elapsedTime.count()/event*(total-event)).Data(),
			GetTime(elapsedTime.count()/event*(total-event)).Data()
		);
	}
	
	if(TPrint::ErrorIf(refresh == 0, __METHOD_NAME__, "Misconfiguration of the progress bar.. (refresh = 0)")) return "";

	TString PBSTR = "||||||||||";

	double percentage = double(event)/double(total);
	int val = (int) (percentage * 100);
	int lpad = (int) (percentage * PBSTR.Length());
	int rpad = PBSTR.Length() - lpad;

	double ratio = ((double) event) / refresh;
	if(int(ratio) != ratio && event != total) return "";

	TString head = (!TString(title).EqualTo("")) ? TString(title) : "";
	TString message = Form("%3d%% [%.*s%*s] (%d/%d%s)", val, lpad, PBSTR.Data(), rpad, "", event, total, progressBarStr.Data());
	if (TPrint::IsQuiet()) return MakeItShorter(head + message);

	TString os;
	os += TPrint::kPurple + MakeItShorter(head) + TPrint::kNoColor;
	os += ": Progress.. ";
	os += MakeItShorter(message);

	return os;
}

TString TPrint::Tab()
{
	if(TPrint::IsDebugEnabled()) return "";
	if(TPrint::IsQuiet()) return "";

	TString os;
	for(int i = 0; i < TPrint::kTabular; i++) os += TPrint::kTabularChar;

	return os;
}

TString TPrint::ReplaceVariadic(const char* str, va_list aptr)
{
	char buf[16384];

	/* A segfault is still occuring when specifying wrong number of parameters..
	   Cannot really be fixed in a smart way.. you have to provide the number of args.. */
	try {

		vsnprintf(buf, sizeof buf, TString(str).ReplaceAll("_%_", " / ").Data(), aptr); // histogram naming convension.. might be improved..
		va_end(aptr);
		return buf;

	} catch (const std::exception& e) {           // reference to the base of a polymorphic object

		std::cerr << e.what() << std::endl;         // information in length_error printed
	}

	return str;
}

TString TPrint::GetInfo(const char* title, const char* str,...)
{
	if(TPrint::IsQuiet()) return "";

	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Info in <" + TString(title) + ">: " : TString(title) + " ";
	TString message = buf.ReplaceAll('\n', '\n' + ROOT::IOPlus::Helpers::Spacer(head.Length()));

	TString os;
	os += TPrint::kGreen + MakeItShorter(head) +TPrint::kNoColor;
	os += MakeItShorter(message);

	return os;
}

void TPrint::Info(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	std::cout << TPrint::CarriageReturn(1);
	std::cout << TPrint::Tab();
	std::cout << TPrint::GetInfo(title, buf);
	std::cout << TPrint::Endl();
	std::cout << std::flush;

	if(!bSingleSkipIncrementTab) TPrint::IncrementTab();
	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

TString TPrint::GetError(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Error in <" + TString(title) + ">: " : TString(title) + " ";
	TString message = buf.ReplaceAll('\n', '\n' + ROOT::IOPlus::Helpers::Spacer(head.Length()));

	TString os;
	os += TPrint::kRed + MakeItShorter(head) +TPrint::kNoColor;
	os += MakeItShorter(message);

	return os;
}

void TPrint::Error(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	std::cerr << TPrint::CarriageReturn(1);
	std::cerr << TPrint::Tab();
	std::cerr << TPrint::GetError(title, buf);

	if(TPrint::IsQuiet()) std::cerr << std::endl;
	else std::cerr << TPrint::Endl();

	std::cerr << std::flush;

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

bool TPrint::ErrorIf(bool condition, const char* title, const char* str, ...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	if(!buf.EqualTo("") && condition) TPrint::Error(title, buf);
	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;

	return condition;
}


TString TPrint::GetWarning(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Warning in <" + TString(title) + ">: " : TString(title) + " ";
	TString message = buf.ReplaceAll('\n', '\n' + ROOT::IOPlus::Helpers::Spacer(head.Length()));

	TString os;
	os += TPrint::kOrange + MakeItShorter(head) +TPrint::kNoColor;
	os += MakeItShorter(message);

	return os;
}

void TPrint::Warning(const char* title, const char* str, ...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	std::cerr << TPrint::CarriageReturn(1);
	std::cerr << TPrint::Tab();
	std::cerr << TPrint::GetWarning(title, buf);

	if(TPrint::IsQuiet()) std::cerr << std::endl;
	else std::cerr << TPrint::Endl();

	std::cerr << std::flush;

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

bool TPrint::WarningIf(bool condition, const char* title, const char* str, ...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);
			buf.ReplaceAll("%", "%%");

	if(!TString(buf).EqualTo("") && condition) TPrint::Warning(title, buf);
	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;

	return condition;
}

TString TPrint::GetMessage(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	if(TPrint::IsQuiet()) return "";

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Message in <" + TString(title) + ">: " : TString(title) + " "; 
	if(TString(title).EqualTo("")) head = head.Strip(TString::kBoth);

	TString message = TString(buf).ReplaceAll('\n', '\n' + TPrint::Tab() + ROOT::IOPlus::Helpers::Spacer(head.Length()));

	TString os;
	os += TPrint::kBlue + MakeItShorter(head) +TPrint::kNoColor;
	os += MakeItShorter(message);

	return os;
}

void TPrint::Message(const char* title, const char* str,...)
{
	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	std::cout << TPrint::CarriageReturn(1);
	std::cout << TPrint::Tab();
	std::cout << TPrint::GetMessage(title, buf);

	if(!bSingleCarriageReturn) std::cout << TPrint::Endl();
	else if (!TPrint::IsSuperQuiet()) std::cout << TPrint::CarriageReturn();
	else if(TPrint::IsQuiet()) std::cout << std::endl;
	else std::cout << TPrint::Endl();

	std::cout << std::flush;

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

void TPrint::Debug(int iDebugLevel, const char* title, const char* str,...)
{
	TString os;

	va_list aptr;
	va_start(aptr, str);
	TString buf = ReplaceVariadic(str, aptr);

	if( TPrint::IsDebugEnabled(iDebugLevel) || iDebugLevel == -1) {

		TString debugLvl = iDebugLevel > 0 ? "(lvl " + TString::Itoa(iDebugLevel,10) + ") " : "";
		TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Debug "+debugLvl+"in <" + TString(title) + ">: " : TString(title) + " ";
		TString message = TString(buf).ReplaceAll('\n', '\n' + TPrint::Tab() + ROOT::IOPlus::Helpers::Spacer(head.Length()));

		os += TPrint::kPurple + MakeItShorter(head) +TPrint::kNoColor;
		os += MakeItShorter(message);

		std::cout << TPrint::CarriageReturn(1);
		std::cout << TPrint::Tab();
		std::cout << os;
		
		if(TPrint::IsQuiet()) std::cout << std::endl;
		else std::cout << TPrint::Endl();

		std::cout << std::flush;

	}

	bSingleSkipIncrementTab = false;
	bSingleCarriageReturn = false;
}

TString TPrint::CreateTemporaryPath(TString format) {

	if(ErrorIf(!format.Contains("XXXXXX"), __METHOD_NAME__, "No XXXXXX found in the path \""+format+"\"")) return "";

	int i = 0;
	int imax = 100;
	TString tmpname;
	do {

		tmpname = format;
		int i = tmpname.Index("XXXXXX");
		while(i > -1) {

				TString rnd = ROOT::IOPlus::Helpers::GetRandomStr(6);
				tmpname = tmpname.Replace(i, 6, rnd);
				i = tmpname.Index("XXXXXX");
		}

	} while( FileExists(tmpname) and i++ < imax);

	if(ErrorIf(i > imax, __METHOD_NAME__, "Too many iterations trying to replace XXXXXX for \""+format+"\".. abort")) return "";

	TPrint::Debug(100, __METHOD_NAME__, "Create temporary file \"" + tmpname + "\" using the pattern \""+format+"\"");
	return tmpname;
}

void TPrint::ReadAndWrite(TString fname, std::vector<TString> regex_pattern, std::vector<TString> replacement)
{
	if(regex_pattern.size() != replacement.size()) {

		int min = TMath::Min(regex_pattern.size(), replacement.size());
		regex_pattern.resize(min);
		replacement.resize(min);
	}

	std::vector<TPRegexp> regex;
	for(int i = 0, N = regex_pattern.size(); i < N; i++)
		regex.push_back(regex_pattern[i]);

	TString bufname = TPrint::CreateTemporaryPath();

	std::ifstream in(fname);
	std::ofstream out(bufname);

	std::string buff;
	while (getline(in, buff)) {

		TString line = TString(buff.c_str());
		for(int i = 0, N = regex.size(); i < N; i++)
			regex[i].Substitute(line, replacement[i]);

		out << line.Data() << std::endl;
	}

	in.close();
	out.close();

	gSystem->Rename(bufname, fname);
}

void TPrint::PressAnyKeyToContinue(int iDebugLevel, const char* title, const char *msg, ...)
{
	va_list aptr;
	va_start(aptr, msg);
	TString buf = ReplaceVariadic(msg, aptr);

    TPrint::Debug(iDebugLevel, title, TString(kOrangeBlink + "Press any key to continue.. " + kNoColor) + buf);
	if(TPrint::IsDebugEnabled(iDebugLevel)) {

		TKeyboard::KeyEvent event = gKeyboard->PressAnyToContinue();
		TPrint::ErasePreviousLines(1);
	}
}

int TPrint::WaitForKeyPress(int iDebugLevel, const char* title, const std::vector<std::vector<unsigned char>> &keycodes, const char* highlight, const char* msg, ...)
{
	va_list aptr;
	va_start(aptr, msg);
	TString buf = ReplaceVariadic(msg, aptr);

    TPrint::Debug(iDebugLevel, title, TString(kOrangeBlink + highlight + kNoColor) + buf);
    if(TPrint::IsDebugEnabled(iDebugLevel)) {
			
		std::vector<TKeyboard::KeyEvent> events;
		for( auto keycode : keycodes )
			events.push_back(TKeyboard::KeyEvent(keycode));

		TKeyboard::KeyEvent event = gKeyboard->Listen(events);
		
		TPrint::ErasePreviousLines(1);
		return std::find(events.begin(), events.end(), event) - events.begin();
	}

	return '\0';
}

void TPrint::ErasePreviousLines(int previousLines)
{
 	while(previousLines-- > 0) {
		std::cout << "\033[F";
		ClearLine();
	}
}

bool TPrint::IsBatch() {
    return !gROOT->IsBatch();
}

bool TPrint::IsInteractive() {
    return isatty(fileno(stdin));
}

void TPrint::ClearPage()
{
	std::cout << "\x1B[2J\x1B[H" << std::flush;
}

std::vector<TString> TPrint::GetTypename(TString prettyFunction) {
	
	std::vector<TString> types;

	// Extract the part between the first '[' and the last ']'
	Ssiz_t start = prettyFunction.Index("[");
	Ssiz_t end = prettyFunction.Index("]", start);
	if (start != kNPOS && end != kNPOS && end > start) {

		TString typeList = prettyFunction(start + 1, end - start - 1);

		// Split the extracted part by commas
		TObjArray* tokens = typeList.Tokenize(",");
		for (int i = 0; i < tokens->GetEntries(); ++i) {
			// Get each token and trim any leading/trailing whitespace
			TString token = ((TObjString*)tokens->At(i))->String();
			token = token.Strip(TString::kBoth);
				
			// Split the token by " = " and get the last entry
			TObjArray* keyValue = token.Tokenize(" = ");
			if (keyValue->GetEntries() > 0) {
				TString type = ((TObjString*)keyValue->At(keyValue->GetEntries() - 1))->String();
				types.push_back(type);
			}
			delete keyValue;
		}
		delete tokens;
	}

	return types;
}

TString TPrint::Redirect(TObject* obj, Option_t* option) {

	TPrint::RedirectTTY(__METHOD_NAME__);
	obj->Print(option);

	TString out, err;
	TPrint::GetTTY(__METHOD_NAME__, &out, &err);
	if(!err.EqualTo("")) {

		if(!out.EqualTo("")) out += '\n';
		out += err;
	}

	out = TPrint::Trim(out);
	return out;
}

void TPrint::TTY(const char* title, const char *out0, const char* err0) {

	TString out = out0;
	TString err = err0;

	TString head = (!TString(title).EqualTo("") && !TString(title).Contains(" ") && TString(title).CountChar(':') > 1) ? "Redirection TTY in <" + TString(title) + ">: " : TString(title) + " ";
	TString shift = ROOT::IOPlus::Helpers::Spacer(MakeItShorter(head).Length() + 2);
	if(!out.EqualTo("")) {

		std::cout << TPrint::CarriageReturn(1);
		std::cout << TPrint::Tab();

		std::cout << TPrint::kOrange;
		if(TPrint::bShortRedirectTTY) std::cout << MakeItShorter(head);
		else std::cout << head;
		std::cout << TPrint::kNoColor;

		TObjArray *array = out.Tokenize('\n');
		for (int i = 0, N = array->GetEntries(); i < N; i++) {

			TString out_i = TPrint::Trim(((TObjString *)(array->At(i)))->String());
			if(out_i.EqualTo("")) continue;

			if(i == 0) std::cout << std::endl;
			std::cout << TPrint::Tab();
			if(TPrint::bShortRedirectTTY) std::cout << '\t' << MakeItShorter(out_i, 100);
			else std::cout << '\t' << out_i;

			if(i != N-1) std::cout << std::endl;
		}
		std::cout << std::endl;

	}

	if(!err.EqualTo("")) {

		std::cerr << TPrint::CarriageReturn(1);
		std::cerr << TPrint::Tab();

		std::cerr << TPrint::kRed;
		if(TPrint::bShortRedirectTTY) std::cerr << MakeItShorter(head);
		else std::cerr << head;
		std::cerr << TPrint::kNoColor;

		TObjArray *array = err.Tokenize('\n');
		for (int i = 0, N = array->GetEntries(); i < N; i++) {

			TString err_i = TPrint::Trim(((TObjString *)(array->At(i)))->String());
			if(err_i.EqualTo("")) continue;

			if(i == 0) std::cerr << std::endl;
			std::cerr << TPrint::Tab();
			std::cerr << '\t' << err_i;
			if(i != N-1) std::cerr << std::endl;
		}
		std::cerr << std::endl;
	}

	TPrint::bShortRedirectTTY = false;
}

void TPrint::PrepareEnvList(bool kForce)
{
	if(kForce) {

		envhashmap.clear();
		envlist.clear();
	}

	if(!TPrint::envlist.size()) {

		TString env = "";
		TString cmd = "env | awk -F'=' '{print $1} '";

		FILE * stream = NULL;
		const int max_buffer = 2048;
		char buffer[max_buffer];
		stream = popen(cmd.Data(), "r");
		if (stream) {

			while (!feof(stream))
				if (fgets(buffer, max_buffer, stream) != NULL) env += buffer;
			pclose(stream);
		}

		TObjArray *fqfnArray = env.Tokenize('\n');
		for (int i = 0; i < fqfnArray->GetEntries(); i++) {

			TString varname = ((TObjString *)(fqfnArray->At(i)))->String();
			TPrint::envlist[varname] = TString(gSystem->Getenv(varname));
		}

		// Create a hashmap in order to replace first the shortest variables, more precisely the one with the smallest ratio varname/value
		std::map<TString, TString>::iterator it;
		for ( it = TPrint::envlist.begin(); it != TPrint::envlist.end(); it++ )
		{
			TString name = it->first;
			TString str = it->second;

			double coeff = ((double) name.Length())/str.Length();
			for(int i = 0, N = envcoeffmap.size(); i <= N; i++) {

				//std::cout << i << std::endl;
				if(i == N) {

					envcoeffmap.push_back(coeff);
					envhashmap.push_back(name);
					break;
				}

				if(envcoeffmap[i] == coeff) {

					if(envhashmap[i].Length() < name.Length()) {

						envcoeffmap.insert(envcoeffmap.begin()+(i+1), coeff);
						envhashmap.insert(envhashmap.begin()+(i+1), name);

					} else {

						envcoeffmap.insert(envcoeffmap.begin()+i, coeff);
						envhashmap.insert(envhashmap.begin()+i, name);
					}

					break;

				} else if(envcoeffmap[i] > coeff) {

					envcoeffmap.insert(envcoeffmap.begin()+i, coeff);
					envhashmap.insert(envhashmap.begin()+i, name);
					break;
				}
			}
		}

		delete fqfnArray;
		fqfnArray = NULL;
	}
}

TString TPrint::ExpandVariables(TString str, bool bRelativePath)
{
	// Get environmental variables
	TPrint::PrepareEnvList();

	if(bRelativePath) {

		if (str.BeginsWith("~/"))
			str = str.Replace(0, 2, "$HOME/");
		if (str.BeginsWith("./"))
			str = str.Replace(0, 2, "$PWD/");
		if (str.BeginsWith("../"))
			str = str.Replace(0, 3, "$PWD/../");
	}

	for (int i = 0, N = TPrint::envlist.size(); i < N; i++)
	{
		std::map<TString, TString>::iterator it = TPrint::envlist.find(envhashmap[i]);

		str.ReplaceAll("${" + it->first +"}", it->second);
		str.ReplaceAll("$(" + it->first +")", it->second);
		str.ReplaceAll("$"  + it->first +"" , it->second);
	}

	return str;
}

TString TPrint::MakeItShorter(TString str, int length, TString color, TString separator)
{
	str.ReplaceAll("file://", "");

	// Get environmental variables
	TPrint::PrepareEnvList();

	if(!TPrint::IsQuiet()) {

		for (int i = 0, N = TPrint::envlist.size(); i < N; i++)
		{
			std::map<TString, TString>::iterator it = TPrint::envlist.find(envhashmap[i]);
			if(it->second.Length() == 0 || it->second.Length() < it->first.Length()+1 || it->first.Length() < 3) continue;             // Skip if variable is too small..

			str.ReplaceAll(it->second, TPrint::kPurple + "${" + it->first +"}"+ color);
		}
	}

	if(TPrint::IsDebugEnabled()) return str;

	// Reduce length of the string..
	TString buf = str;
	int diff = str.Length() - length;
	if(length != -1 && diff > 0 && length > 4) {

		buf  = color + str(0, str.Length()/2-diff/2);
		buf += separator;
		buf += str(str.Length()/2+diff/2, str.Length());
	}
	
	TString pwd = TString(gSystem->pwd()) + "/";
	buf.ReplaceAll(pwd, "./");
	return buf + TPrint::kNoColor;
}

TString TPrint::ReadTTY(TString fname)
{
    TString out = "";

    if(fname.EqualTo("") || fname.EqualTo("/dev/tty") || fname.EqualTo("/dev/pts/0"))
        return "";

    FILE *fp = fopen(fname, "r");
    if(fp == NULL)
        return "";

    char * line = NULL;
    size_t len = 0;

    while(getline(&line, &len, fp) != -1)
        out += TString(line);

    if(fileno(fp) != kStdErr_dup && fileno(fp) != kStdOut_dup) fclose(fp);

    remove(fname);
    return out;
}


bool TPrint::ValidTTY(TString fname)
{
    fname = Readlink(fname);
    if(fname.EqualTo("")) return 0;
    if(fname.EqualTo("/dev/tty")) return 0;

    TString fname0;
    fname0 = Readlink(stdout);
    if(fname.EqualTo(fname0)) return 0;

    fname0 = Readlink(stderr);
    if(fname.EqualTo(fname0)) return 0;

    for(int i = 0, N = kStdOut.size(); i < N; i++) {
        fname0 = Readlink(kStdOut[i]);
        if(fname.EqualTo(fname0))
            return 0;
    }

    for(int i = 0, N = kStdErr.size(); i < N; i++) {
        fname0 = Readlink(kStdErr[i]);
        if(fname.EqualTo(fname0))
            return 0;
    }

    return 1;
}

int TPrint::OpenTTY(int desc, TString fname)
{
    if(fname.EqualTo("")) {
		
        if(desc == 1) fname = CreateTemporaryPath("/tmp/stdout.XXXXXX");
        else if(desc == 2) fname = CreateTemporaryPath("/tmp/stderr.XXXXXX");
    }

    if(TPrint::WarningIf(!ValidTTY(fname), __METHOD_NAME__, "Invalid TTY-"+TString::Itoa(desc,10)+" name provided.. \""+fname+"\""))
	return -1;

    TPrint::Debug(20, __METHOD_NAME__, "Debugging mode detected.. TTY-"+TString::Itoa(desc,10)+" shunted.");
    if(TPrint::IsDebugEnabled()) return 0;

    if(desc == 1) {

        int fd = kStdOut.size() ? dup(1) : kStdOut_dup;
        kStdOut.push_back(fd);
        kStdOut_fname.push_back(fname);

        // Prepare the new fd
        FILE *fOut = fopen(fname, "w");
        if(TPrint::ErrorIf(fOut == NULL, __METHOD_NAME__, "Cannot open \""+fname+"\"")) return -1;
        fd = fileno(fOut);

        // Redirect stream
        fflush(stdout);
		dup2(fd, 1);
        if(fd != kStdOut_dup) fclose(fOut);

        return kStdOut.size();
    }

    if(desc == 2) {

        int fd = kStdErr.size() ? dup(2) : kStdErr_dup;
        kStdErr.push_back(fd);
        kStdErr_fname.push_back(fname);

        // Prepare the new fd
        FILE *fErr = fopen(fname, "w");
        if(TPrint::ErrorIf(fErr == NULL, __METHOD_NAME__, "Cannot open \""+fname+"\"")) return -1;
        fd = fileno(fErr);

        // Redirect stream
        fflush(stderr);
		dup2(fd, 2);
		if(fd != kStdErr_dup) fclose(fErr);

        return kStdErr.size();
    }

    return -1;
}

TString TPrint::CloseTTY(int desc)
{
    TString fname = "";

    TPrint::Debug(20, __METHOD_NAME__, "Debugging mode detected.. TTY-"+TString::Itoa(desc,10)+" shunted.");
    if(TPrint::IsDebugEnabled()) return "";

    if(desc == 1 && kStdOut.size()) {

        FILE *fp = stdout;
        fname = Readlink(fp);

        int fd = kStdOut.back();
        kStdOut.pop_back();

        fflush(stdout);
        dup2(fd, 1);
        clearerr(stdout);

		fname = kStdOut_fname.back();

		kStdOut_fname.pop_back();
		kTrash.push_back(fname.Data());

        if(fd != kStdOut_dup)
			close(fd);
    }

    if(desc == 2 && kStdErr.size()) {

        FILE *fp = stderr;
        fname = Readlink(fp);

        int fd = kStdErr.back();
        kStdErr.pop_back();

        fflush(stderr);
        dup2(fd, 2);
        clearerr(stderr);

		fname = kStdErr_fname.back();
		kStdErr_fname.pop_back();
		kTrash.push_back(fname.Data());

        if(fd != kStdErr_dup)
			close(fd);
    }

    return fname;
}


TString TPrint::ListTTY(int desc)
{
    TString list = "Descriptor #" + TString::Itoa(desc,10) + ":\n";
    if(desc == 1) {

        for(int i = 0, N = kStdOut.size(); i < N; i++) {

            int fd = kStdOut[i];
	    	TString pts = Readlink(fd);
	   		TString fname = kStdOut_fname[i];

            if(fname.EqualTo("")) list += "* tty="+TString::Itoa(i,10)+"; no file (fd = " +TString::Itoa(fd,10) + ")" + "\n";
            else list += "* tty="+TString::Itoa(i,10)+"; "+pts+" --> "+fname+" (fd = " +TString::Itoa(fd,10) + ")" + "\n";
        }

        int fd = fileno(stdout);
		TString pts = Readlink(fd);
        list += "* stdout; "+pts+" (fd = " +TString::Itoa(fd,10) + ")";

    } else if(desc == 2) {

        for(int i = 0, N = kStdErr.size(); i < N; i++) {

            int fd = kStdErr[i];
			TString pts = Readlink(fd);
			TString fname = kStdErr_fname[i];

            if(fname.EqualTo("")) list += "* tty="+TString::Itoa(i,10)+"; no file (fd = " +TString::Itoa(fd,10) + ")" + "\n";
            else list += "* tty="+TString::Itoa(i,10)+"; "+pts+" --> "+fname+" (fd = " +TString::Itoa(fd,10) + ")" + "\n";
        }

        int fd = fileno(stderr);
        TString fname = Readlink(fd);
        list += "* stderr; "+fname+" (fd = " +TString::Itoa(fd,10) + ")";

    } else {

        list += "* nothing found";
    }

    return list;
}

int TPrint::RedirectTTY(const char* title, bool make_it_shorter)
{
    TPrint::Debug(20, __METHOD_NAME__, "Redirection TTY("+TString::Itoa(iTTY,10)+") starts here");

    iTTY++;
    if(TPrint::WarningIf(OpenTTY(1) < 0, __METHOD_NAME__, "Redirection TTY-1("+TString::Itoa(iTTY,10)+") could not be opened in <"+ TString(title) +">")) {
		iFailedTTY++;
		return iTTY;
    }

    if(TPrint::WarningIf(OpenTTY(2) < 0, __METHOD_NAME__, "Redirection TTY-2("+TString::Itoa(iTTY,10)+") could not be opened in <"+ TString(title) +">")) {
        CloseTTY(1);
		iFailedTTY++;
		return iTTY;
    }

    bShortRedirectTTY = make_it_shorter;
    return iTTY;
}

void TPrint::GetTTY(const char *title, TString *out, TString *err)
{
	UNUSED(title);

    if(TPrint::iFailedTTY-- > 0) {
		TPrint::Warning(__METHOD_NAME__, "Redirection TTY("+TString::Itoa(iTTY,10)+") would have ended here");
		return;
    }

    if(TPrint::iTTY <= 0) {
        TPrint::Warning(__METHOD_NAME__, "Redirection TTY("+TString::Itoa(iTTY,10)+") unexpectedly ended here");
		return;
    }

    TString fOut = CloseTTY(1);
    if(out != NULL) *out = ReadTTY(fOut);
	if(FileExists(fOut)) remove(fOut);

    TString fErr = CloseTTY(2);
    if(err != NULL) *err = ReadTTY(fErr);
	if(FileExists(fErr)) remove(fErr);

    TPrint::Debug(20, __METHOD_NAME__, "Redirection TTY("+TString::Itoa(--iTTY,10)+") ended here");
}

void TPrint::TTY(const char* title, bool bOut, bool bErr)
{
    TString out,err;
    GetTTY(title, &out, &err);
    if(!bOut) out = "";
    if(!bErr) err = "";

    TPrint::TTY(title, out, err);
    bSingleSkipIncrementTab = false;
    bSingleCarriageReturn = false;
}

void TPrint::DumpTTY(const char* title)
{
    GetTTY(title, NULL, NULL);
    TPrint::Debug(20, __METHOD_NAME__, "Redirection TTY("+TString::Itoa(iTTY,10)+") has been dumped");
}
