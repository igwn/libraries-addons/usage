#include "ROOT/IOPlus/Helpers.h"
#include <TPRegexp.h>
#include <TFormula.h>
#include <TF1.h>

ClassImp(ROOT::IOPlus::Helpers)

/** @cond */
int ROOT::IOPlus::Helpers::nInterpretFormula = 0;
std::atomic<int> ROOT::IOPlus::Helpers::randomseed_type = ROOT::IOPlus::E_DEFAULT;
std::atomic<long> ROOT::IOPlus::Helpers::randomseed = -1;
/** @endcond */

template <typename T>
std::vector<T> ROOT::IOPlus::Helpers::InverseTransformSampling(int N, TF1 *pdf)
{
        std::vector<T> px(N, 0);

        for(int i = 0; i < N; i++) {
                px[i] = pdf->GetRandom();
        }

        return px;
}

template <typename T>
std::vector<T> ROOT::IOPlus::Helpers::GetRandomVector(int N, TString pdfFormula, std::vector<T> pdfParams, double xMin, double xMax)
{
        TF1 *pdf = new TF1("random_vector["+GetRandomStr()+"]", pdfFormula, xMin, xMax);
        pdf->SetParameters(&pdfParams[0]);

        std::vector<T> px = GetRandomVector<T>(N, pdf);

        pdf->Delete();
        return px;
}

double ROOT::IOPlus::Helpers::InterpretFormula(TString scale_str, const std::vector<TString> &v0, const std::vector<TString> &e0)
{
        // THIS FUNCTION SHOULD NOT BE USED IN A LOOP :(
        // It create a TFormula each time it is called. Although it is deleted in the end.. ROOT does not like it and slow down step by step.
        // It might not be a big deal when used in a small loop, but if N > 10000.. it becomes problematic.

        TString str0 = scale_str;
        str0.ToLower();
        if(str0.EqualTo("nan")) return NAN;
        if(str0.EqualTo("")) return 1;

        /*if(nInterpretFormula == 20000) {

                std::cerr << "You called InterpretFormula about 20000 times." << std::endl;
                std::cerr << "This command is creating and deleting a TFormula each time.." << std::endl;
                std::cerr << "ROOT performances might be reduced after creating too many TObject.." << std::endl;
        }*/

        // Replace possible variables..
        int G = 0;
        for(int i = 0, N = v0.size(); i < N; i++) {

                scale_str.ReplaceAll("[c"+TString::Itoa(i,10)+"]", v0[i]);
                scale_str.ReplaceAll("["+TString::Itoa(G,10)+"]", v0[i]);
                G++;
        }

        for(int i = 0, N = e0.size(); i < N; i++) {

                scale_str.ReplaceAll("[e"+TString::Itoa(i,10)+"]", e0[i]);
                scale_str.ReplaceAll("["+TString::Itoa(G,10)+"]", e0[i]);
                G++;
        }

        gErrorIgnoreLevel = kBreak;
        TPRegexp pattern1("\\w+\\.");  // Matches "word."
        TPRegexp pattern2("\\.\\w+");  // Matches ".word"
    
        // Check if the string contains the pattern
        if (pattern1.Match(scale_str) || pattern2.Match(scale_str)) {
                std::cerr << "Error: Invalid formula passed as argument cannot be resolved \"" << scale_str << "\"" << std::endl;
                return NAN;
        }
        
        TFormula *scale = new TFormula(scale_str, scale_str);
        nInterpretFormula++;
        gErrorIgnoreLevel = kPrint;
        if(!scale->IsValid()) {
                std::cerr << "Error: Invalid formula passed as argument cannot be resolved \"" << scale_str << "\"" << std::endl;
                return NAN;
        }

        double newscale = scale->Eval(0);
        delete scale;
        scale = NULL;

        return newscale;
}