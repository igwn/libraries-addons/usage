#include "ROOT/IOPlus/Impl/TArchiveImpl.h"

TArchive::Impl::Impl(const char *aName) {
    
    this->aName = aName;
    this->Reset();
}

TArchive::Impl::~Impl()
{
    Close();
}

archive *TArchive::Impl::GetHandler() 
{ 
    if(this->fHandler == nullptr) this->Open();
    return this->fHandler;
}

Int_t TArchive::Impl::Open(Int_t index)
{
    if(this->fHandler == NULL) {

        this->fHandler = archive_read_new();
        archive_read_support_format_all(this->fHandler);
        archive_read_support_filter_all(this->fHandler);

        this->fCurrentStatus = archive_read_open_filename(this->fHandler, aName, 10240);
        this->fCurrentPos = -1;
    }
    
    if(this->fCurrentStatus == ARCHIVE_OK && index > 0) this->Seekg(index);
    return this->fCurrentStatus;
}

void TArchive::Impl::Close()
{
    if (this->fHandler) {
        archive_read_free(this->fHandler);
        this->fHandler = nullptr;
    }

    this->fCurrentEntry = nullptr;
    this->fCurrentStatus = ARCHIVE_FATAL;
    this->fCurrentPos = -1;
}

Int_t TArchive::Impl::Reset() {

    this->Close();
    return this->Open();    
}

bool TArchive::Impl::ExtractMember(Int_t index, TString destination)
{
    this->Reset();
    this->Seekg(index);
    
    FILE* fp = fopen(destination, "wb");
    if (!fp) {
        fprintf(stderr, "Error: Failed to create local file: `%s`\n", destination.Data());        
        return false;
    }

    ssize_t size;
    char buff[8192]; // Buffer for reading data

    // Read data from the archive entry and write it to the file
    while ((size = archive_read_data(this->GetHandler(), buff, sizeof(buff))) > 0) {
        
        if (fwrite(buff, 1, size, fp) != size) {
            fprintf(stderr, "Error: Failed to write data to file: `%s`\n", destination.Data());
            fclose(fp);
            return false;
        }
    }

    if (size < 0) {
        // An error occurred while reading data from the archive
        fprintf(stderr, "Error: Failed to read data from archive: `%s`\n", archive_error_string(this->GetHandler()));
        fclose(fp);
        return false;
    }

    fclose(fp);
    return true;    
}

const char *TArchive::Impl::GetCompression()
{
    return archive_filter_name(this->GetHandler(), 0);
}

Int_t TArchive::Impl::GetStatus()
{
    return this->fCurrentStatus;
}

int TArchive::Impl::GetN()
{
    this->Open();

    for(;;) {
        if(!this->Next()) break;
    }

    return this->fCurrentPos;
}

void TArchive::Impl::Seekg(int index)
{
    if(this->fCurrentPos > index) this->Reset();
    else if(!this->fHandler) this->Open();

    do {

        if(this->fCurrentPos == index) break;

    } while(this->Next());
}

int TArchive::Impl::Tellg()
{
    this->Open();
    return this->fCurrentPos;
}

bool TArchive::Impl::Next()
{
    this->fCurrentStatus = archive_read_next_header(this->GetHandler(), &this->fCurrentEntry);
    this->fCurrentPos++;

    if(this->fCurrentStatus == ARCHIVE_EOF) return false;
    if(this->fCurrentStatus == ARCHIVE_OK) return true;

    return false;
}

archive_entry *TArchive::Impl::GetEntry()
{
    return this->fCurrentEntry;
}

Int_t TArchive::Impl::Extract(const char *destinationPath) {

    this->Reset();

    struct archive* extract = archive_write_disk_new();
    int flags = ARCHIVE_EXTRACT_TIME;
        flags |= ARCHIVE_EXTRACT_PERM;
        flags |= ARCHIVE_EXTRACT_ACL;
        flags |= ARCHIVE_EXTRACT_FFLAGS;

    archive_write_disk_set_options(extract, flags);
    archive_write_disk_set_standard_lookup(extract);

    struct stat info; // Create output directory if it doesn't exists, print error if cannot create.
    if( stat( destinationPath, &info ) != 0 && gSystem->mkdir(destinationPath, true) != 0) {
            std::cout << "Failed to create directory `" << destinationPath << "`" << std::endl;
            return 0;
    }

    while ( this->Next() ) {

        struct archive_entry* entry = this->GetEntry();

        TString currentFile    = TString(archive_entry_pathname(entry));
        TString fullOutputPath = TString(destinationPath) + "/" + currentFile;
        archive_entry_set_pathname(entry, fullOutputPath);

        int r = archive_write_header(extract, entry);
        if (r < ARCHIVE_OK) {

            std::cerr << "Warning: " << archive_error_string(extract) << std::endl;

        } else if (archive_entry_size(entry) > 0) {

            // Extract the file
            const void* buff;
            size_t size;
            la_int64_t offset;

            while (archive_read_data_block(this->GetHandler(), &buff, &size, &offset) == ARCHIVE_OK) {
                
                r = archive_write_data_block(extract, buff, size, offset);
                if (r < ARCHIVE_OK) {
                
                    std::cerr << "Failed to write data: " + std::string(archive_error_string(extract)) << std::endl;
                    return r;
                }
            }
        }
        archive_write_finish_entry(extract);
    }

    archive_write_close(extract);
    archive_write_free(extract);

    return ARCHIVE_OK;
}

TString TArchive::Impl::GetEntryName(bool prependArchiveName)
{
    archive_entry *entry = this->GetEntry();
    if(entry == nullptr) return "";

    TString entryName = archive_entry_pathname(entry);
    if (entryName.BeginsWith("./")) {
        entryName = entryName(2, entryName.Length() - 2);
    }

    return prependArchiveName ? (this->aName + ":" + entryName) : entryName;
}
std::vector<TString> TArchive::Impl::GetEntryNames(bool prependArchiveName) {

    this->Reset();
    std::vector<TString> contents;
    while ( this->Next() ) {
        contents.emplace_back(this->GetEntryName(prependArchiveName));
    }

    return contents;
}

Int_t TArchive::Impl::Write(const char *aName, const CompressionFormat &format, TArchiveBuffer members)
{    
    TString tmpName;
    FILE *fp = ROOT::IOPlus::Helpers::TempFileName("tmparchive-", &tmpName);
    fclose(fp);

    struct archive *a;
    struct archive_entry *aEntry;

    int fd;
    ssize_t len;
    char buff[8192];
    int result = 0;

    // Create and open the archive for writing
    a = archive_write_new();

    archive_write_set_format_pax_restricted(a); 

    switch (format) {
        case CompressionFormat::GZIP:
            archive_write_add_filter_gzip(a);
            break;
        case CompressionFormat::BZIP2:
            archive_write_add_filter_bzip2(a);
            break;
        case CompressionFormat::XZ:
            archive_write_add_filter_xz(a);
            break;
        case CompressionFormat::LZ4:
            archive_write_add_filter_lz4(a);
            break;
        case CompressionFormat::ZSTD:
            archive_write_add_filter_zstd(a);
            break;
        case CompressionFormat::NONE:
        default:
            archive_write_add_filter_none(a);
            break;
    }


    // Open the archive file for writing
    if (archive_write_open_filename(a, tmpName) != ARCHIVE_OK) {
        std::cerr << "Could not open file: " << tmpName << " for writing." << std::endl;
        archive_write_free(a);
        return -2;
    }
    
    std::map<TString, archive*> sourceArchives; // To keep track of open archives for cleanup
    std::map<TString, int> sourceArchivePositions; // To keep track of the last position

    // Iterate over the map and add files to the archive
    for (const auto &member : members) {

        TString memberPath    = member.first;
        if (memberPath.BeginsWith(".")) 
            memberPath = memberPath(1, memberPath.Length() - 1);
        if (memberPath.BeginsWith("/")) 
            memberPath = memberPath(1, memberPath.Length() - 1);

        TString localPath     = member.second;
        TString archivePath   = TFileReader::FileName(localPath);
        TString archiveMember = TFileReader::Obj(localPath);
        if (archiveMember.BeginsWith("/")) 
            archiveMember = archiveMember(1, archiveMember.Length() - 1);

        if(archiveMember.EqualTo("")) {

            // Handle absolute paths (file on disk)
            struct archive_entry *aEntry = archive_entry_new();
            archive_entry_set_pathname(aEntry, TString("./") + memberPath.Data()); // Use the key as the entry name
            archive_entry_set_filetype(aEntry, AE_IFREG);
            archive_entry_set_perm(aEntry, 0644);

            FILE *fp = fopen(localPath.Data(), "rb");
            if (!fp) {

                std::cerr << "Warning: Local file `" << localPath << "` not found.. cannot add to archive `" << aName << "`" << std::endl;
                archive_entry_free(aEntry);
                continue; // Skip this entry if the file cannot be opened
            }

            fseek(fp, 0, SEEK_END);
            size_t fileSize = ftell(fp);
            fseek(fp, 0, SEEK_SET);

            archive_entry_set_size(aEntry, fileSize);
            archive_write_header(a, aEntry);

            // Write the file content to the archive
            char buffer[8192];
            size_t bytesRead;
            while ((bytesRead = fread(buffer, 1, sizeof(buffer), fp)) > 0) {
                archive_write_data(a, buffer, bytesRead);
            }

            fclose(fp);
            archive_entry_free(aEntry);

        } else {

            if (sourceArchives.find(archivePath) == sourceArchives.end()) {

                archive *sourceArchive = archive_read_new();
                archive_read_support_format_all(sourceArchive);
                archive_read_support_filter_all(sourceArchive);

                if (archive_read_open_filename(sourceArchive, archivePath.Data(), 10240) == ARCHIVE_OK) {
                    sourceArchives[archivePath] = sourceArchive;
                    sourceArchivePositions[archivePath] = 0;
                } else {
                    std::cerr << "Warning: Failed to read `" << archivePath << "`. skip.." << std::endl;
                    archive_read_free(sourceArchive); // Ensure proper cleanup
                    continue; // Skip if we cannot open the archive
                }
            }

            archive *sourceArchive = sourceArchives[archivePath];
            archive_entry *sourceArchiveEntry = NULL;

            bool found = false, first = true;
            int sourceArchivePosition = sourceArchivePositions[archivePath];
            do { 

                // Reset the archive to the beginning if we've reached the end without finding the entry
                if (archive_read_next_header(sourceArchive, &sourceArchiveEntry) != ARCHIVE_OK) {

                    // Restart from the beginning of the archive
                    archive_read_close(sourceArchive);
                    archive_read_open_filename(sourceArchive, archivePath.Data(), 10240);
                    
                    sourceArchives[archivePath] = sourceArchive;
                    sourceArchivePosition = 0;
                    continue;
                }

                TString currentEntryName = archive_entry_pathname(sourceArchiveEntry);
                if (currentEntryName.BeginsWith("./")) currentEntryName = currentEntryName(2, currentEntryName.Length() - 2);
                if (currentEntryName.EqualTo(archiveMember)) {
                    
                    archive_write_header(a, sourceArchiveEntry);

                    ssize_t size;
                    while ((size = archive_read_data(sourceArchive, buff, sizeof(buff))) > 0) {
                        archive_write_data(a, buff, size);
                    }

                    sourceArchivePositions[archivePath] = sourceArchivePosition;
                    found = true;
                    break; // Exit after handling the specific entry
                }

                sourceArchivePosition++;
                first = false;

            } while (first || sourceArchivePositions[archivePath] != sourceArchivePosition);

            if (!found) {
                std::cerr << "Warning: member path `" << archiveMember << "` not found in `" << archivePath << "`" << std::endl;
            }
        }
    }

    // Finalize writing to targetArchive
    archive_write_close(a);
    archive_write_free(a);

    // Cleanup: Close and free all source archives
    std::map<TString, archive*>::iterator it;
    for ( it = sourceArchives.begin(); it != sourceArchives.end(); it++ )
    {
        archive *aa = it->second;
        archive_read_close(aa);
        archive_read_free(aa);
    }
    sourceArchives.clear(); // Clear the vector after cleanup

    gSystem->Rename(tmpName, aName);
    return result;
}