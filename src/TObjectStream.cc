#include "TObjectStream.h"

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

int TObjectStream::Initialize(const char *token)
{
    this->token = token;
    this->connect = (this->Open() == this);

    return this->connect;
}

void TObjectStream::Close()
{
    this->connect = false;
    TPSocket::Close();
}

TObjectStream* TObjectStream::Open(const char *host, const char *service, const char *token, Int_t tcpwindowsize)
{
    TPrint::Message(__METHOD_NAME__, "Try to connect.. %s on %s", host, gClock->Local().Data());

    TObjectStream *stream = NULL;
    for(int i = 0, authAttempts = 0; authAttempts < maxAuthAttempts; i++) {
    
        TObjectStream *stream = NULL;
        try {
    
            auto errorHandler = SetErrorHandler(TObjectStream::ErrorHandler);
            stream = new TObjectStream(host, service, token, tcpwindowsize);
            SetErrorHandler(errorHandler);

        } catch(const std::runtime_error& e) { authAttempts++; }

        if(stream != NULL) {
            
            if(stream->IsOpen()) return stream;
            
            delete stream;
            stream = NULL;
        }

        gSystem->Sleep(TObjectStream::timeout);
    }

    TPrint::Error(__METHOD_NAME__, "Too many failed attempts to connect %s.. aborted on %s.", host, gClock->Local().Data());
    return NULL;
}

Long_t TObjectStream::Ticks()
{
    return this->ticks;
}

Long_t TObjectStream::LastUpdate()
{
    this->ticks++;
    return (Long_t) 1000*(TTimeStamp().AsDouble() - this->lastUpdate);
}

TObjectStream* TObjectStream::Open(const char *host, Int_t port, const char *token, Int_t tcpwindowsize)
{
    return TObjectStream::Open(host, gSystem->GetServiceByPort(port), token, tcpwindowsize);
}

TObjectStream* TObjectStream::Open()
{
    if (!this->IsValid()) {

        TObjectStream *stream = NULL;
        try {
    
            auto errorHandler = SetErrorHandler(TObjectStream::ErrorHandler);
            stream = new TObjectStream(this->GetName(), this->fService, this->token, this->fTcpWindowSize);       
            SetErrorHandler(errorHandler);

        } catch(const std::runtime_error& e) { }

        if(stream != NULL) {

            if(stream->IsOpen()) return stream;
            delete stream;
        }

        TPrint::Error(__METHOD_NAME__, "Failed to reach %s at %s", this->GetName(), gClock->Local().Data());
        return NULL;
      }

    if(!this->Authenticate()) {
        TPrint::Error(__METHOD_NAME__, "Authentication failed on `%s` (received on %s) ", this->GetName(), gClock->Local().Data());
        throw std::runtime_error("Connection refused.");
    }

    TPrint::Message(__METHOD_NAME__, "Connection established on %s with `%s`", gClock->Local().Data(), this->GetName());
    return this;
}

TMessage* TObjectStream::FindObject(const char *objName, TClass *cl)
{
    return this->FindObject(objName, cl->GetName());
}

TMessage* TObjectStream::FindObject(const char *objName, const char *cl)
{
    if(!this->IsOpen()) return NULL;

    TMessage *message = this->Send(objName);
    if (!TString(cl).EqualTo("") && !message->GetClass()->InheritsFrom(cl)) return NULL;
    
    return message;
}

bool TObjectStream::IsOpen()
{ 
    return connect == SUCCESS && this->IsValid();
}

bool TObjectStream::Authenticate()
{
    bool success;
    TMessage *message = this->Send(token);
    if(message == NULL) return false;
    
    message->ReadBool(success);
    return success;
}

TMessage* TObjectStream::Send(const char *msg)
{
    TMessage *message = NULL;
    try {

        this->lastUpdate = TTimeStamp().AsDouble();
        this->ticks = 0;

        auto errorHandler = SetErrorHandler(TObjectStream::ErrorHandler);
        TPSocket::Send(msg);

        if (this->Recv(message) <= 0) return NULL;

        SetErrorHandler(errorHandler);

    } catch(const std::runtime_error& e) {
        this->Close();
    }

    return message;
}

TObject* TObjectStream::ReadObject(const char *objName, TClass *cl)
{
    return this->ReadObject(objName, cl->GetName());
}

TObject* TObjectStream::ReadObject(const char *objName, const char *cl)
{
    TMessage *message = this->FindObject(objName);
    if(message == NULL) return NULL;

    return this->ReadObject(message);
}

TObject* TObjectStream::ReadObject(TMessage *message)
{
    if(!this->IsOpen()) return NULL;
    return message->ReadObject(message->GetClass());
}

void TObjectStream::ErrorHandler(int level, Bool_t abort, const char *location, const char *msg)
{
    if (level >= ::kError) throw std::runtime_error(msg);
    DefaultErrorHandler(level, abort, location, msg);
}

void TObjectStream::Print(Option_t *opt)
{
    this->Print(opt);
}

ClassImp(TObjectStream)
