/**
 **********************************************
 *
 * \file libRIOPlus.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all functions;

#pragma link C++ class TKeyboard+;
#pragma link C++ global gKeyboard;
#pragma link C++ class TPrint+;
#pragma link C++ global gPrint;
#pragma link C++ class TPicasso;
#pragma link C++ global gPicasso;

#pragma link C++ enum class Clock;
#pragma link C++ class TClockAbstract;

#pragma link C++ class TClock<Clock::Unix>+;
#pragma link C++ global gClock;
#pragma link C++ global gClockUnix;
#pragma link C++ class TClock<Clock::GPS>+;
#pragma link C++ global gClockGPS;
#pragma link C++ class TClock<Clock::TAI>+;
#pragma link C++ global gClockTAI;

#pragma link C++ class TCli+;
#pragma link C++ class TVarexp+;
#pragma link C++ class TFileReader+;

#pragma link C++ class ROOT::IOPlus::Helpers+;
#pragma link C++ enum ROOT::IOPlus::Seed+;

#pragma link C++ class TObjectStream+;
#pragma link C++ class TObjectStreamer+;

#pragma link C++ enum class ExecutionPolicy;
#pragma link C++ enum class TTensorType;
#pragma link C++ enum class TTensorDevice;
#pragma link C++ enum class TTensorLayout;
#pragma link C++ enum class TTensorNorm;

#pragma link C++ class ROOT::IOPlus::TTensorT<int>+;
#pragma link C++ class ROOT::IOPlus::TTensorT<int>::Iterator+;
#pragma link C++ class ROOT::IOPlus::TTensorT<short>+;
#pragma link C++ class ROOT::IOPlus::TTensorT<short>::Iterator+;
#pragma link C++ class ROOT::IOPlus::TTensorT<float>+;
#pragma link C++ class ROOT::IOPlus::TTensorT<float>::Iterator+;
#pragma link C++ class ROOT::IOPlus::TTensorT<std::complex<float>>+;
#pragma link C++ class ROOT::IOPlus::TTensorT<std::complex<float>>::Iterator+;
#pragma link C++ class ROOT::IOPlus::TTensorT<double>+;
#pragma link C++ class ROOT::IOPlus::TTensorT<double>::Iterator+;
#pragma link C++ class ROOT::IOPlus::TTensorT<std::complex<double>>+;
#pragma link C++ class ROOT::IOPlus::TTensorT<std::complex<double>>::Iterator+;

#pragma link C++ class TTensorC+;
#pragma link C++ class TTensorD+;
#pragma link C++ class TTensorF+;

#ifdef Archive_FOUND
    #pragma link C++ class TArchive+;
#endif

#ifdef Curl_FOUND
    #pragma link C++ class TCurl+;
    #pragma link C++ class TCurlFile+;
    #pragma link C++ class TCurlRequest+;
    #pragma link C++ class TCurlResponse+;
#endif

#endif
