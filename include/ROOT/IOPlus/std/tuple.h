#pragma once

#include <iostream>
#include <tuple>
#include <vector>

namespace std
{
    //
    // Tuple shortcut for slicing
    template <typename T>
    using tuple2 = std::tuple<T, T>;
    template <typename T>
    struct is_tuple2_int64 : std::false_type {};
    template <>
    struct is_tuple2_int64<std::tuple<int64_t, int64_t>> : std::true_type {};

    template <typename T>
    using tuple3 = std::tuple<T, T, T>;
    template <typename T>
    struct is_tuple3_int64 : std::false_type {};
    template <>
    struct is_tuple3_int64<std::tuple<int64_t, int64_t, int64_t>> : std::true_type {};
    template <typename T>
    std::vector<tuple3<T>> make_tuple3(const std::vector<tuple2<T>>& v) {
            std::vector<tuple3<T>> result;

            for (const auto& t : v) {
                    tuple3<T> tuple;
                    std::get<0>(tuple) = std::get<0>(t);
                    std::get<1>(tuple) = 0;
                    std::get<2>(tuple) = std::get<1>(t);

                    result.push_back(tuple);
            }

            return result;
    }

    template <typename T>
    using tuple4 = std::tuple<T, T, T, T>;
    template <typename T>
    struct is_tuple4_int64 : std::false_type {};
    template <>
    struct is_tuple4_int64<std::tuple<int64_t, int64_t, int64_t, int64_t>> : std::true_type {};
    template <typename T>
    std::vector<tuple4<T>> make_tuple4(const std::vector<tuple2<T>>& v) {
            std::vector<tuple4<T>> result;

            for (const auto& t : v) {
                    tuple4<T> tuple;
                    std::get<0>(tuple) = std::get<0>(t);
                    std::get<1>(tuple) = 0;
                    std::get<2>(tuple) = std::get<1>(t);
                    std::get<3>(tuple) = 1;

                    result.push_back(tuple);
            }

            return result;
    }

    template <typename T>
    std::vector<tuple4<T>> make_tuple4(const std::vector<tuple3<T>>& v) {
            std::vector<tuple4<T>> result;

            for (const auto& t : v) {
                    tuple4<T> tuple;
                    std::get<0>(tuple) = std::get<0>(t);
                    std::get<1>(tuple) = std::get<1>(t);
                    std::get<2>(tuple) = std::get<2>(t);
                    std::get<3>(tuple) = 1;

                    result.push_back(tuple);
            }

            return result;
    }    
}