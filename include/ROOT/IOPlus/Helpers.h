#pragma once

#include <Riostream.h>
#include <algorithm>
#include <TMath.h>
#include <TSystem.h>

#include <cmath>
#include <sys/stat.h>
#include <execinfo.h>
#include <cxxabi.h>
#include <dlfcn.h>

#include "Complex.h"
#include <vector>
#include <TMatrixT.h>
#include <TRandom.h>
#include <sys/time.h>
#include <unistd.h>
#include <limits.h>
#include <stdlib.h>
#include <random>
#include <iomanip>
#include <sstream>

#include <TPad.h>
#include <regex>
#include <chrono>

#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>

#include <TColor.h>
#include <TROOT.h>

#include <iostream>
#include <regex>

class TF1; // Forward declaration due to conflit with Vc (same for TFormula)
// $CONDA_PREFIX/conda-bld/debug_1737860167735/_h_env/include/Vc/scalar/vector.h: In instantiation of 'class Vc_1::Vector<std::chrono::duration<int, std::ratio<604800> >, Vc_1::VectorAbi::Scalar>':
// $CONDA_PREFIX/conda-bld/debug_1737860167735/_h_env/include/date/date.h:7827:52:   required from here
// $CONDA_PREFIX/conda-bld/debug_1737860167735/_h_env/include/Vc/scalar/vector.h:50:42: error: static assertion failed: Vector<T> only accepts arithmetic builtin types as template parameter T.
//    50 |     static_assert(std::is_arithmetic<T>::value,
//       |                                          ^~~~~

namespace ROOT { namespace IOPlus {

        //
        // Complex calculation
        using complex32 = std::complex<float>;
        using complex64 = std::complex<double>;
        const std::complex<double> i = std::complex<double>(0.0,1.0);

        enum Seed {E_DEFAULT = -1, E_TIME = -2, E_MICROTIME = -3, E_TIMEPID = -4, E_MICROTIMEPID = -5, E_URANDOM = -6, E_RANDOMD = -7};
        
        class Helpers {

                public:
                
                Helpers() { }
                Helpers(const Helpers&) = delete;
                Helpers& operator=(const Helpers&) = delete;

                virtual ~Helpers() { }

                static inline bool IsPrintable(const char *str, size_t length)
                {
                        for(int i = 0; i < (int) length; i++)
                                if(!isprint(str[i])) return false;

                        return true;
                }

                static inline bool IsAbsolutePath(const char *path) {
                
                        #ifdef _WIN32
                        return path.Length() > 2 && path[1] == ':';
                        #else
                        return strcmp(path, "") != 0 && path[0] == '/';
                        #endif
                }

                static inline int IsBinary(const void *data, size_t size) {

                        const unsigned char *bytes = (const unsigned char *)data;
                        for (size_t i = 0; i < size; ++i) {
                                // Check if the byte is outside the ASCII range
                                if (bytes[i] < 0x20 && bytes[i] != 0x09 && bytes[i] != 0x0A && bytes[i] != 0x0D) {
                                        return 1; // Binary
                                }
                        }
                        return 0; // ASCII
                }

                template <typename T>
                static inline bool IsNaN(const T x)
                {
                        #if __cplusplus >= 201103L
                                using std::isnan;
                        #endif

                        return isnan(x);
                }

                template <typename T>
                static inline bool IsInf(const T x)
                {
                        #if __cplusplus >= 201103L
                        using std::isinf;
                        #endif
                        
                        return isinf(x);
                }

                static inline std::time_t Time(const std::string& str, bool is_dst = false, const std::string& format = "%Y-%b-%d %H:%M:%S")
                {
                        std::tm t{};
                        t.tm_isdst = is_dst ? 1 : 0;
                        std::istringstream ss(str);
                        ss >> std::get_time(&t, format.c_str());
                        return mktime(&t);
                }

                static inline TString Spacer(int length, char c = ' ')
                {
                        TString space = "";
                        for(int i = 0; i < length; i++) space += c;
                        return space;
                }

                static inline void HexDump(unsigned char* buffer, unsigned int length)
                {        
                        unsigned int b=0, c=0;
                        int s, rem;

                        // b is a counter for the number of bytes (half the number of hex digits)
                        std::cout << std::endl << "     PAYLOAD HEXDUMP:" << std::endl;
                        while (b < length) {

                                std::cout << std::endl << "     x:" << b;
                                for (; (b%16<15) && (b<length); b++) {
                                        if (0 == b % 2) printf(" ");
                                        std::cout << std::hex << buffer[b];
                                }

                                if (b < length) std::cout << std::hex << buffer[b++];
                                else { // print a number of spaces to align the remaining text
                                        rem = b % 16;
                                        for (s=0; s < 44 - ((rem*2) + (rem/2) + 1); s++)
                                                std::cout << " ";
                                }

                                for (;(c%16<15) && (c<length); c++) if (isprint(buffer[c])) std::cout << buffer[c];
                                if (c<length && isprint(buffer[c])) std::cout << buffer[c++];
                        }
                }

                static inline TString StripLeadingZeros(const TString& number) {

                        TString strippedNumber = number;
                        
                        // Remove leading zeros by finding the first non-zero character
                        int firstNonZeroIdx = 0;
                        while (firstNonZeroIdx < strippedNumber.Length() && strippedNumber[firstNonZeroIdx] == '0') {
                                ++firstNonZeroIdx;
                        }
                        
                        // If the string is all zeros, return "0"
                        if (firstNonZeroIdx == strippedNumber.Length()) {
                                return "0";
                        }
                        
                        return strippedNumber(firstNonZeroIdx, strippedNumber.Length() - firstNonZeroIdx);
                }

                static inline TString FormatNumber(const TString& number, int groupSize = 3, const TString& spacer = "'") {

                        // Early exit if groupSize is zero
                        if (groupSize == 0) {
                                return number;
                        }

                        TString formatted;
                        int len = number.Length();
                        
                        // Calculate how many leading zeros to add for padding
                        int padding = (groupSize - (len % groupSize)) % groupSize;
                        
                        // Create a new TString that includes the necessary leading zeros
                        TString paddedNumber;
                        for (int i = 0; i < padding; ++i) {
                                paddedNumber.Append('0');
                        }
                        paddedNumber.Append(number);
                        
                        // Update the length of the padded number
                        len = paddedNumber.Length();

                        // Group the digits and add the spacer
                        for (int i = 0; i < len; ++i) {
                                formatted.Append(paddedNumber[i]);
                                
                                // Add the spacer after every groupSize digits, except after the last group
                                if ((i + 1) % groupSize == 0 && i != len - 1) {
                                formatted.Append(spacer);
                                }
                        }

                        return formatted;
                }

                static inline Color_t Invert(const Color_t& kColor)
                {
                        TColor *color = gROOT->GetColor(kColor);
                        return TColor::GetColor(1 - color->GetRed(),  1 - color->GetGreen(),  1 - color->GetBlue());
                }

                static inline double microtime()
                {
                        return static_cast<double>(
                                std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count()
                        ) / 1e6;
                }

                template<typename T>
                static inline T Sign(T a) { return TMath::Sign(T(1), a); }

                static inline TString Implode(char delim, std::vector<TString> v)
                {
                        return Implode(TString(delim).Data(), v);
                }
                
                static inline TString Implode(const char *delim, std::vector<TString> v)
                {
                        if(v.size() == 0) return "";
                        if(v.size() == 1) return v[0];
                        
                        TString x;
                        for(int i = 0, N = v.size(); i < N; i++)
                        {
                                x += (TString) ((i > 0) ? delim : "");
                                x += v[i];
                        }

                        return x;
                }

                static inline const char* Implode(const TString delim, std::vector<const char *> x) 
                { 
                        std::vector<TString> X(x.size());
                        for(int i = 0, N = X.size(); i < N; i++) 
                                X[i] = TString(x[i]);

                        return Implode(delim, X).Data();
                }


                template <typename T>
                static inline T StripNaN( const T& a, T replacement = 0 ) { return IsNaN(a) ? replacement : a; }
                template <typename T>
                static inline std::vector<T> StripNaN( const std::vector<T>& v, T replacement = 0 )
                {
                        std::vector<T> result;
                        for(int i = 0, N = v.size(); i < N; i++) {
                        
                                result.push_back(IsNaN(v[i]) ? replacement : v[i]);
                        }

                        return result;
                }
                template <typename T>
                static inline std::vector<std::vector<T>> StripNaN( const std::vector<std::vector<T>>& v, T replacement = 0 )
                {
                        std::vector<std::vector<T>> result;
                        for(int i = 0, N = v.size(); i < N; i++) {

                                result.push_back({});
                                for(int j = 0, Nj = v.size(); j < Nj; j++) {

                                        result[i].push_back(IsNaN(v[i][j]) ? replacement : v[i][j]);
                                }
                        }

                        return result;
                }
                template <typename T>
                static inline std::vector<std::vector<std::vector<T>>> StripNaN( const std::vector<std::vector<std::vector<T>>>& v, T replacement = 0 )
                {
                        std::vector<std::vector<std::vector<T>>> result;
                        for(int i = 0, Ni = v.size(); i < Ni; i++) {

                                result.push_back({});
                                for(int j = 0, Nj = v.size(); j < Nj; j++) {

                                        result[i].push_back({});
                                        for(int k = 0, Nk = v.size(); k < Nk; k++) {

                                                result[i][j].push_back(IsNaN(v[i][j][k]) ? replacement : v[i][j][k]);
                                        }
                                }
                        }

                        return result;
                }
                
                template <typename T>
                static inline T StripInf( const T& a, T replacement = 0 ) { return IsInf(a) ? replacement : a; }
                template <typename T>
                static inline std::vector<T> StripInf( const std::vector<T>& v, T replacement = 0 )
                {
                        std::vector<T> result;
                        for(int i = 0, N = v.size(); i < N; i++) {
                                result.push_back(IsInf(v[i]) ? replacement : v[i]);
                        }

                        return result;
                }
                
                template <typename T>
                static inline std::vector<std::vector<T>> StripInf( const std::vector<std::vector<T>>& v, T replacement = 0 )
                {
                        std::vector<std::vector<T>> result;
                        for(int i = 0, Ni = v.size(); i < Ni; i++) {

                                result.push_back({});
                                for(int j = 0, Nj = v.size(); j < Nj; j++) {

                                        result[i].push_back(IsInf(v[i][j]) ? replacement : v[i][j]);
                                }
                        }

                        return result;
                }
                template <typename T>
                static inline std::vector<std::vector<std::vector<T>>> StripInf( const std::vector<std::vector<std::vector<T>>>& v, T replacement = 0 )
                {
                        std::vector<std::vector<std::vector<T>>> result;
                        for(int i = 0, Ni = v.size(); i < Ni; i++) {

                                result.push_back({});
                                for(int j = 0, Nj = v.size(); j < Nj; j++) {

                                        result[i].push_back({});
                                        for(int k = 0, Nk = v.size(); k < Nk; k++) {

                                                result[i][j].push_back(IsInf(v[i][j][k]) ? replacement : v[i][j][k]);
                                        }
                                }
                        }

                        return result;
                }

                template<typename T>
                static inline TH1* Histogram  (TString name, TString title, const std::vector<double> &x, const std::vector<T> &content, const std::vector<T> &contentErr = std::vector<T>({}))
                {
                        if(content.size() == 0) return NULL;
                        if(contentErr.size() > 0 && content.size() != contentErr.size()) return NULL;
                        if(x.size() != content.size()+1) {
                                std::cerr << "Unexpected vector size: x.size() != content.size()+1 (" << x.size() << " != " << (content.size()+1) << ")" << std::endl;
                                return NULL;
                        }

                        TH1* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH1*) new TH1C(name, title, x.size()-1, &x[0]);
                        if(std::is_same<T,short>::value)
                                h = (TH1*) new TH1S(name, title, x.size()-1, &x[0]);
                        if(std::is_same<T,int>::value)
                                h = (TH1*) new TH1I(name, title, x.size()-1, &x[0]);
                        if(std::is_same<T,float>::value)
                                h = (TH1*) new TH1F(name, title, x.size()-1, &x[0]);
                        if(std::is_same<T,double>::value)
                                h = (TH1*) new TH1D(name, title, x.size()-1, &x[0]);

                        if(h == NULL) return NULL;
                        h->Sumw2(false);

                        for(int i = 1, Ni = content.size()+1, contentErrN = contentErr.size(); i < Ni; i++)
                        {
                                int bin = h->FindBin(x[i-1]);
                                h->SetBinContent(bin, StripNaN(StripInf(content[i-1])));
                                if(contentErrN > 0) h->SetBinError(bin, StripNaN(StripInf(contentErr[i-1])));
                        }
                        
                        return h;
                }

                template<typename T>
                static inline TH1* Histogram  (TString name, TString title, const std::pair<std::vector<double>, std::vector<double>> &xy, const std::vector<T> &content, const std::vector<T> &contentErr = std::vector<T>({}))
                {
                        return Histogram(name, title, std::make_tuple(xy.first, xy.second), content, contentErr);
                }

                template<typename T>
                static inline TH1* Histogram  (TString name, TString title, const std::tuple<std::vector<double>, std::vector<double>> &xy, const std::vector<T> &content, const std::vector<T> &contentErr = std::vector<T>({}))
                {
                        const std::vector<double> &x = std::get<0>(xy);
                        const std::vector<double> &y = std::get<1>(xy);

                        if(content.size() == 0) return NULL;
                        if(contentErr.size() > 0 && content.size() != contentErr.size()) return NULL;
                        if(x.size() != content.size()+1) return NULL;
                        if(y.size() != content.size()+1) return NULL;

                        TH1* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH1*) new TH2C(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                        if(std::is_same<T,short>::value)
                                h = (TH1*) new TH2S(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                        if(std::is_same<T,int>::value)
                                h = (TH1*) new TH2I(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                        if(std::is_same<T,float>::value)
                                h = (TH1*) new TH2F(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);
                        if(std::is_same<T,double>::value)
                                h = (TH1*) new TH2D(name, title, x.size()-1, &x[0], y.size()-1, &y[0]);

                        if(h == NULL) return NULL;
                        h->Sumw2(false);

                        for(int i = 1, Ni = content.size()+1, contentErrN = contentErr.size(); i < Ni; i++)
                        {
                                int bin = h->FindBin(x[i-1], y[i-1]);
                                h->SetBinContent(bin, StripNaN(StripInf(content[i-1])));
                                if(contentErrN > 0) h->SetBinError(bin, StripNaN(StripInf(contentErr[i-1])));
                        }

                        return h;
                }

                template<typename T>
                static inline TH1* Histogram  (TString name, TString title, const std::tuple<std::vector<double>, std::vector<double>, std::vector<double>> &xyz, const std::vector<T> &content, const std::vector<T> &contentErr = std::vector<T>({}))
                {
                        const std::vector<double> &x = std::get<0>(xyz);
                        const std::vector<double> &y = std::get<1>(xyz);
                        const std::vector<double> &z = std::get<2>(xyz);

                        if(content.size() == 0) return NULL;
                        if(contentErr.size() > 0 && content.size() != contentErr.size()) return NULL;
                        if(x.size() != content.size()+1) return NULL;
                        if(y.size() != content.size()+1) return NULL;
                        if(z.size() != content.size()+1) return NULL;

                        TH1* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH1*) new TH3C(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                        if(std::is_same<T,short>::value)
                                h = (TH1*) new TH3S(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                        if(std::is_same<T,int>::value)
                                h = (TH1*) new TH3I(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                        if(std::is_same<T,float>::value)
                                h = (TH1*) new TH3F(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);
                        if(std::is_same<T,double>::value)
                                h = (TH1*) new TH3D(name, title, x.size()-1, &x[0], y.size()-1, &y[0], z.size()-1, &z[0]);

                        if(h == NULL) return NULL;
                        h->Sumw2(false);

                        for(int i = 1, Ni = content.size()+1, contentErrN = contentErr.size(); i < Ni; i++)
                        {
                                int bin = h->FindBin(x[i-1],y[i-1],z[i-1]);
                                h->SetBinContent(bin, StripNaN(StripInf(content[i-1])));
                                if(contentErrN > 0) h->SetBinError(bin, StripNaN(StripInf(contentErr[i-1])));
                        }

                        return h;
                }

                template<typename T>
                static inline TH1* Histogram  (TString name, TString title, double scaleX, const std::vector<T> &content, const std::vector<T> &contentErr = std::vector<T>({}))
                {
                        if(content.size() == 0) return NULL;
                        if(contentErr.size() > 0 && content.size() != contentErr.size()) return NULL;
                        
                        TH1* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH1*) new TH1C(name, title, content.size(), 0, content.size()*scaleX);
                        if(std::is_same<T,short>::value)
                                h = (TH1*) new TH1S(name, title, content.size(), 0, content.size()*scaleX);
                        if(std::is_same<T,int>::value)
                                h = (TH1*) new TH1I(name, title, content.size(), 0, content.size()*scaleX);
                        if(std::is_same<T,float>::value)
                                h = (TH1*) new TH1F(name, title, content.size(), 0, content.size()*scaleX);
                        if(std::is_same<T,double>::value)
                                h = (TH1*) new TH1D(name, title, content.size(), 0, content.size()*scaleX);

                        if(h == NULL) return NULL;
                        h->Sumw2(false);

                        for(int i = 1, Ni = content.size()+1, contentErrN = contentErr.size(); i < Ni; i++)
                        {
                                h->SetBinContent(i, StripNaN(StripInf(content[i-1])));
                                if(contentErrN > 0) h->SetBinError(i, StripNaN(StripInf(contentErr[i-1])));
                        }

                        return h;
                }
                template<typename T>
                static inline TH2* Histogram  (TString name, TString title, double scaleX, double scaleY, const std::vector<std::vector<T>> &content, const std::vector<std::vector<T>> &contentErr = std::vector<std::vector<T>>())
                {
                        if(content.size() < 1) return NULL;
                        if(content[0].size() < 1) return NULL;
                        if(contentErr.size() > 0 && content.size() != contentErr.size()) return NULL;

                        TH2* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH2*) new TH2C(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                        if(std::is_same<T,short>::value)
                                h = (TH2*) new TH2S(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                        if(std::is_same<T,int>::value)
                                h = (TH2*) new TH2I(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                        if(std::is_same<T,float>::value)
                                h = (TH2*) new TH2F(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                        if(std::is_same<T,double>::value)
                                h = (TH2*) new TH2D(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY);
                        
                        if(h == NULL) return NULL;
                        h->Sumw2(false);
                        
                        for(int i = 1, Ni = content.size()+1, contentErrN = contentErr.size(); i < Ni; i++)
                        {
                                for(int j = 1, Nj = content[i].size()+1, contentErrN = contentErr[i].size(); j < Nj; j++)
                                {
                                        h->SetBinContent(i, j, StripNaN(StripInf(content[i-1][j-1])));
                                        if(contentErrN > 0) h->SetBinError(i, j, StripNaN(StripInf(contentErr[i-1][j-1])));
                                }
                        }

                        return h;
                }

                template<typename T>
                static inline TH3* Histogram  (TString name, TString title, double scaleX, double scaleY, double scaleZ, const std::vector<std::vector<std::vector<T>>> &content, const std::vector<std::vector<std::vector<T>>> &contentErr = std::vector<std::vector<std::vector<T>>>())
                {
                        if(content.size() < 1) return NULL;
                        if(content[0].size() < 1) return NULL;
                        if(contentErr.size() > 0 && content.size() != contentErr.size()) return NULL;

                        TH3* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH3*) new TH3C(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                        if(std::is_same<T,short>::value)
                                h = (TH3*) new TH3S(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                        if(std::is_same<T,int>::value)
                                h = (TH3*) new TH3I(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                        if(std::is_same<T,float>::value)
                                h = (TH3*) new TH3F(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                        if(std::is_same<T,double>::value)
                                h = (TH3*) new TH3D(name, title, content.size(), 0, content.size()*scaleX, content[0].size(), 0, content[0].size()*scaleY, content[0][0].size(), 0, content[0][0].size()*scaleZ);
                        
                        if(h == NULL) return NULL;
                        h->Sumw2(false);

                        for(int i = 1, Ni = content.size()+1, contentErrN = contentErr.size(); i < Ni; i++)
                        {
                                for(int j = 1, Nj = content[i].size()+1, contentErrN = contentErr[i].size(); j < Nj; j++)
                                {
                                        for(int k = 1, Nk = content[i][j].size()+1, contentErrN = contentErr[i][j].size(); k < Nk; k++)
                                        {
                                                h->SetBinContent(i, j, k, StripNaN(StripInf(content[i-1][j-1][k-1])));
                                                if(contentErrN > 0) h->SetBinError(i, j, k, StripNaN(StripInf(contentErr[i-1][j-1][k-1])));
                                        }
                                }
                        }

                        return h;
                }

                template<typename T>
                static inline TH1* Histogram(TString name, TString title, int nbinsx, double xmin, double xmax, const std::vector<T> &x, const std::vector<T> &w = {})
                {
                        if(w.size() > 0 && w.size() != x.size()) return NULL;

                        TH1* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH1*) new TH1C(name, title, nbinsx, xmin, xmax);
                        if(std::is_same<T,short>::value)
                                h = (TH1*) new TH1S(name, title, nbinsx, xmin, xmax);
                        if(std::is_same<T,int>::value)
                                h = (TH1*) new TH1I(name, title, nbinsx, xmin, xmax);
                        if(std::is_same<T,float>::value)
                                h = (TH1*) new TH1F(name, title, nbinsx, xmin, xmax);
                        if(std::is_same<T,double>::value)
                                h = (TH1*) new TH1D(name, title, nbinsx, xmin, xmax);
                        
                        if(h == NULL) return NULL;

                        h->Sumw2(w.size());
                        for(int i = 0, N = x.size(); i < N; i++) {
                                h->Fill(x[i], w.size() ? w[i] : 1);
                        }

                        return h;
                }

                template<typename T>
                static inline TH2* Histogram(TString name, TString title, int nbinsx, double xmin, double xmax, int nbinsy, double ymin, double ymax, const std::vector<T> &x, const std::vector<T> &y, const std::vector<T> &w = {})
                {
                        if(y.size() != x.size()) return NULL;
                        if(w.size() > 0 && w.size() != x.size()) return NULL;

                        TH2* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH2*) new TH2C(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                        if(std::is_same<T,short>::value)
                                h = (TH2*) new TH2S(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                        if(std::is_same<T,int>::value)
                                h = (TH2*) new TH2I(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                        if(std::is_same<T,float>::value)
                                h = (TH2*) new TH2F(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                        if(std::is_same<T,double>::value)
                                h = (TH2*) new TH2D(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax);
                        
                        if(h == NULL) return NULL;

                        h->Sumw2(w.size());
                        for(int i = 0, N = x.size(); i < N; i++) {
                                h->Fill(x[i],y[i], w.size() ? w[i] : 1);
                        }

                        return h;
                }

                template<typename T>
                static inline TH3* Histogram(TString name, TString title, int nbinsx, double xmin, double xmax, int nbinsy, double ymin, double ymax, int nbinsz, double zmin, double zmax, const std::vector<T> &x, const std::vector<T> &y, const std::vector<T> &z, std::vector<T> w = {})
                {
                        if(y.size() != x.size()) return NULL;
                        if(z.size() != x.size()) return NULL;
                        if(w.size() > 0 && w.size() != x.size()) return NULL;

                        TH3* h = NULL;
                        if(std::is_same<T,char>::value)
                                h = (TH3*) new TH3C(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);
                        if(std::is_same<T,short>::value)
                                h = (TH3*) new TH3S(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);
                        if(std::is_same<T,int>::value)
                                h = (TH3*) new TH3I(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);
                        if(std::is_same<T,float>::value)
                                h = (TH3*) new TH3F(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);
                        if(std::is_same<T,double>::value)
                                h = (TH3*) new TH3D(name, title, nbinsx, xmin, xmax, nbinsy, ymin, ymax, nbinsz, zmin, zmax);

                        if(h == NULL) return NULL;
                        h->Sumw2(w.size());

                        for(int i = 0, N = x.size(); i < N; i++) {
                                h->Fill(x[i],y[i],z[i], w.size() ? w[i] : 1);
                        }

                        return h;
                }
                
                template<typename T>
                static inline TH1* Histogram  (TString name, TString title, const std::vector<T> &content) { return Histogram(name, title, 1, content); }

                template<typename T>
                static inline TH2* Histogram  (TString name, TString title, const std::vector<std::vector<T>> &content) { return Histogram(name, title, 1, 1, content); }
                
                template<typename T>
                static inline TH3* Histogram  (TString name, TString title, const std::vector<std::vector<std::vector<T>>> &content) { return Histogram(name, title, 1, 1, 1, content); }
                
                static inline TString Ucfirst(TString str) 
                {
                        TString c = str(0, 1);
                                c.ToUpper();

                        return c + TString(str(1,str.Length()-1));
                }

                static inline std::vector<TString> Explode(const TString delim, TString x)
                {
                        std::vector<TString> v;

                        for(int pos = -1; (pos = x.Index(delim)) != -1;) {

                                v.push_back(x(0, pos));
                                x = x(pos+delim.Length(), x.Length());
                        }

                        v.push_back(x);

                        return v;
                }

                static inline int CountPads(TVirtualPad *pad)
                {
                        if (!pad) return 0;

                        // Get the list of primitives in the pad
                        TList* list = pad->GetListOfPrimitives();
                        if (!list) return 0;

                        int subPadCount = 0;

                        TIter next(list);
                        TObject* obj;
                        while ((obj = next())) {
                                
                                if (obj->InheritsFrom("TPad")) subPadCount++;
                        }

                        return subPadCount;
                }

                static inline TAxis* GetAxis(TH1* hist, int i = 0) {
                
                        if (!hist) return nullptr;
                        switch(i) {

                                case 0:
                                        if(!hist->InheritsFrom("TH1") && !hist->InheritsFrom("TH2") && !hist->InheritsFrom("TH3"))
                                                return NULL;
                                        return hist->GetXaxis();

                                case 1: 
                                        if(!hist->InheritsFrom("TH2") && !hist->InheritsFrom("TH3"))
                                                return NULL;
                                        return hist->GetYaxis();

                                case 2: 
                                        if(!hist->InheritsFrom("TH3"))
                                                return NULL;
                                        return hist->GetZaxis();  
                        }
                        
                        return nullptr;
                }

                static inline TVirtualPad* GetPad(TVirtualPad* pad, int index)
                {
                        if (!pad) return nullptr;

                        TList* list = pad->GetListOfPrimitives();
                        if (!list) return nullptr;

                        if (index < 0 || index >= list->GetSize()) return nullptr;

                        TIter next(list);
                        TObject* obj;

                        int currentIndex = 0;
                        while ((obj = next())) {

                                if (obj->InheritsFrom("TPad")) {

                                        if (currentIndex == index) return dynamic_cast<TPad*>(obj);
                                        currentIndex++;
                                }
                        }

                        return nullptr;
                }

                static inline int GetPadIndex(TVirtualPad* pad)
                {
                        if (!pad) return -1;
                        
                        TVirtualPad* parentPad = dynamic_cast<TPad*>(pad->GetMother());
                        if (!parentPad) return -1;

                        // Get the list of primitives (objects) in the parent pad
                        TList* list = parentPad->GetListOfPrimitives();
                        if (!list) return -1;

                        // Iterate through the list and find the index of the child pad
                        TIter next(list);
                        TObject* obj;

                        int index = 0;
                        while ((obj = next())) {

                                if (obj->InheritsFrom("TPad")) {
                                        
                                        TVirtualPad* subPad = dynamic_cast<TPad*>(obj);
                                        if (subPad == pad) return index;

                                        index++;
                                }
                        }

                        return -1;
                }

                static int nInterpretFormula;
                static double InterpretFormula(TString scale_str, const std::vector<TString> &v0 = {}, const std::vector<TString> &e0 = {});

                static inline double MathMod(double a, double b) {
                        return a - b * TMath::Floor(a / b);
                }

                static inline double InterpretFormula(TString scale_str, const std::vector<double> &v0, const std::vector<double> &e0) {

                        std::vector<TString> v, e;
                        for(int i = 0; i < (int) v0.size(); i++) v.push_back(Form("%f", v0[i]));
                        for(int i = 0; i < (int) e0.size(); i++) e.push_back(Form("%f", e0[i]));
                        return InterpretFormula(scale_str, v, e);
                }

                static inline std::vector<std::string> Explode(std::string const & s, char delim)
                {
                        std::vector<std::string> result;
                        std::istringstream iss(s);

                        for (std::string token; std::getline(iss, token, delim); )
                                result.push_back(std::move(token));

                        return result;
                }

                template <typename T>
                static inline bool EpsilonEqualTo(const std::complex<T>& a, const std::complex<T>& b, const T &epsilon = std::numeric_limits<T>::epsilon())
                {
                        return std::abs(a - b) <= epsilon * std::max(std::abs(a), std::abs(b)) || std::abs(a - b) <= std::numeric_limits<T>::min();
                }

                template <typename T>
                static inline bool EpsilonEqualTo(const std::vector<T> &a, const std::vector<T> &b, const T &epsilon = std::numeric_limits<T>::epsilon())
                {
                        if(a.size() != b.size())
                                return false;
                                
                        for(int i = 0, N = a.size(); i < N; i++)
                                if(!EpsilonEqualTo(a[i], b[i], epsilon)) return false;

                        return true;
                }

                template <typename T>
                static inline bool EpsilonEqualTo(const std::vector<std::complex<T>> &z1, const std::vector<std::complex<T>> &z2, const T &epsilon = std::numeric_limits<T>::epsilon())
                {
                        if(z1.size() != z2.size())
                                return false;
                                
                        for(int i = 0, N = z1.size(); i < N; i++)
                                if(!EpsilonEqualTo(z1[i], z2[i], epsilon)) return false;

                        return true;
                }

                template <typename T>
                static inline bool EpsilonEqualTo(const T &a, const T &b, const T &epsilon = std::numeric_limits<T>::epsilon()) {
                        return std::abs(a - b) <= epsilon * std::max(std::abs(a), std::abs(b)) || std::abs(a - b) <= std::numeric_limits<T>::min();
                }

                template <typename T>
                static inline bool EpsilonEqualTo(const TMatrixT<T>& M, const T &b, const T &epsilon = std::numeric_limits<T>::epsilon())
                {
                        for (int i = 0; i < M.GetNrows(); i++) {

                                for (int j = 0; j < M.GetNcols(); j++) {
                                
                                        if(!EpsilonEqualTo(M(i, j), b, epsilon)) return false;
                                }
                        }

                        return true;
                }

                template <typename T>
                static inline bool EpsilonEqualTo(const std::complex<T>& z, const T &r, const T &epsilon = std::numeric_limits<T>::epsilon()) { return EpsilonEqualTo(z.real(), std::complex<T>(r,0), epsilon); }
                template <typename T>
                static inline bool EpsilonEqualTo(const std::vector<T> &a, const T &b, const T &epsilon = std::numeric_limits<T>::epsilon()) { return EpsilonEqualTo(a, std::vector<T>(a.size(), b), epsilon); }
                template <typename T>
                static inline bool EpsilonEqualTo(const std::vector<T> &a, const std::complex<T>& b, const T &epsilon = std::numeric_limits<T>::epsilon()) { return EpsilonEqualTo(std::complex<T>(a,0), b, epsilon); }
                template <typename T>
                static inline bool EpsilonEqualTo(const std::vector<std::complex<T>> &a, const T &b, const T &epsilon = std::numeric_limits<T>::epsilon()) { return EpsilonEqualTo(a, std::vector<std::complex<T>>(a.size(), std::complex<T>(b,0)), epsilon); }
                template <typename T>
                static inline bool EpsilonEqualTo(const std::vector<std::complex<T>> &a, const std::complex<T>& b, const T &epsilon = std::numeric_limits<T>::epsilon()) { return EpsilonEqualTo(a, std::vector<std::complex<T>>(a.size(), b), epsilon); }
                template <typename T>
                static inline bool EpsilonEqualTo(const std::vector<std::complex<T>> &z, const std::vector<T> &r, const T &epsilon = std::numeric_limits<T>::epsilon())
                {
                        if(z.size() != r.size())
                                return false;
                                
                        for(int i = 0, N = z.size(); i < N; i++)
                                if(!EpsilonEqualTo(z[i].real(), std::complex<double>(r[i], 0), epsilon)) return false;

                        return true;
                }

                template <typename T>
                static bool IsInteger(const T x) { return EpsilonEqualTo((double) ((int) x), x); }
                static inline double Epsilon() { return std::numeric_limits<double>::epsilon(); }

                static inline std::vector<double>::iterator find_epsilon(std::vector<double>::iterator vbegin, std::vector<double>::iterator vend, double d) {

                        for(std::vector<double>::iterator it = vbegin; it != vend; it++)
                                if(EpsilonEqualTo(*it, d)) return it;

                        return vend;
                }

                static inline unsigned int read_urandom()
                {
                        union {
                                int value;
                                char cs[sizeof(unsigned int)];
                        } u;

                        std::ifstream rfin("/dev/urandom");
                        rfin.read(u.cs, sizeof(u.cs));
                        rfin.close();

                        return u.value;
                }

                static inline std::vector<unsigned int> random_device(int i)
                {
                        std::random_device r;
                        std::seed_seq seq{r(), r(), r(), r(), r(), r(), r(), r()};
                        std::vector<std::uint32_t> seeds(i);
                
                        seq.generate(seeds.begin(), seeds.end());
                        return seeds;
                }

                static std::atomic<long> randomseed;
                static inline unsigned int GetSeed() { return GetRandomSeed(); }
                static std::atomic<int>  randomseed_type;
                static inline bool SetSeed(long type = E_DEFAULT) {

                        if(randomseed < 0 && type < 0 ) {

                                int seed = TString(gSystem->Getenv("ESCALADE_SEED")).Atoi();
                                type = seed ? seed : E_RANDOMD;
                        }
                        
                        if(type == E_DEFAULT) {
                                return 0;
                        }

                        if(randomseed_type != type) {

                                timeval t1;
                                gettimeofday(&t1, NULL);

                                randomseed_type = type;
                                switch(randomseed_type) {
                                
                                        case E_TIME:
                                        randomseed = time(NULL);
                                        break;
                                        
                                        case E_TIMEPID:
                                        randomseed = time(NULL)*getpid();
                                        break;
                                        
                                        case E_MICROTIME:
                                        randomseed = t1.tv_usec*t1.tv_sec;
                                        break;
                                        
                                        case E_MICROTIMEPID:
                                        randomseed = t1.tv_usec*t1.tv_sec*getpid();
                                        break;

                                        case E_URANDOM:
                                        randomseed = read_urandom();
                                        break;
                                        
                                        case E_RANDOMD:
                                        randomseed = random_device(1)[0];
                                        break;
        
                                        default:
                                        randomseed = TMath::Abs(type); // Seed >=0
                                }

                                gRandom->SetSeed(randomseed);
                                srand(randomseed);

                                return 1;
                        }

                        return 0;
                }

                static inline bool SetRandomSeed(long type = E_DEFAULT) { return SetSeed(type); }

                template <typename T>
                static inline T GCD (T A, T B, double eps = 1e0) {
                        int precision = TMath::Power(10, TMath::Abs(TMath::Log10(eps)));
                        return std::gcd((int) (A*precision), (int) (B*precision))/precision;
                }

                template <typename T>
                static inline T LCM (T A, T B, double eps=1e0) {
                        int precision = TMath::Power(10, TMath::Abs(TMath::Log10(eps)));
                        return std::lcm((int) (A * precision), (int) (B * precision))/precision;
                        
                }

                static inline unsigned int GetRandomSeed() {

                        SetSeed();
                        return randomseed;
                }

                static inline TString GetRandomStr( size_t length = 6, int type = E_DEFAULT)
                {
                        SetSeed(type);
                        auto randchar = []() -> char
                        {
                                const char charset[] =
                                "0123456789"
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz";
                                const size_t max_index = (sizeof(charset) - 1);
                                return charset[ rand() % max_index ];
                        };

                        std::string str(length,0);
                        std::generate_n( str.begin(), length, randchar );
                        return TString(str);
                }

                static inline TString TempDirectory(int length = 0)
                {
                        TString tempDirectory = gSystem->TempDirectory();

                        struct stat buffer;
                        if (gSystem->AccessPathName(tempDirectory, kWritePermission) != 0) {
                                return nullptr;
                        }

                        TString _fName = tempDirectory;
                        do {
                                _fName = tempDirectory + GetRandomStr(length) + "/";
                                gSystem->mkdir(_fName, true);
                        
                        } while (stat(_fName.Data(), &buffer) != 0);

                        return _fName;
                }

                static inline FILE* TempFileName(const char *baseName = "temp-", TString *fName = NULL)
                {
                        TString tempDirectory = gSystem->TempDirectory();

                        struct stat buffer;
                        if (gSystem->AccessPathName(tempDirectory, kWritePermission) != 0) {
                                return nullptr;
                        }

                        FILE* _fp = NULL;
                        TString _fName;
                        do {
                        
                                _fName = tempDirectory + baseName + GetRandomStr();
                                _fp = fopen(_fName.Data(), "wb");
                        
                        } while (stat(_fName.Data(), &buffer) != 0);

                        if (fName != nullptr) *fName = _fName;

                        return _fp;
                }

                template <typename T>
                static std::vector<T> InverseTransformSampling(int N, TF1 *pdf);
                template <typename T>
                static std::vector<T> GetRandomVector(int N, TString pdfFormula, std::vector<T> pdfParams, double xMin, double xMax);
                template <typename T>
                static inline std::vector<T> GetRandomVector(int N, TF1 *pdf) { return InverseTransformSampling<T>(N, pdf); }

                static inline TString CamelToSnake(TString camel, char separator = '_')
                {
                        // Empty String
                        std::string snake = "";
                        
                        // Append first character(in lower case)
                        // to snake string
                        char c = tolower(camel[0]);
                        snake += (char(c));
                        
                        // Traverse the string from
                        // ist index to last index
                        for (int i = 1; i < camel.Length(); i++) {
                        
                                char ch = camel[i];
                        
                                // Check if the character is upper case
                                // then append '_' and such character
                                // (in lower case) to snake string
                                if (isupper(ch)) {
                                snake  =  snake + separator;
                                snake += char(tolower(ch));
                                }
                        
                                // If the character is lower case then
                                // add such character into snake string
                                else {
                                snake = snake + ch;
                                }
                        }
                        
                        // return the snake
                        return TString(snake.c_str());
                }
                
                static inline TString SnakeToCamel(TString _snake)
                {
                        std::string snake = _snake.Data();
                        for (auto it = snake.begin(); it != snake.end(); it++)
                        {
                                if (*it == '-' || *it == '_') {

                                        it = snake.erase(it);
                                        *it = toupper(*it);
                                }
                        }

                        return TString(snake.c_str());
                }
                
                template <typename T = double, typename Tx = T>
                static inline T GetRandom(Tx max = 1, long type = E_DEFAULT)
                {
                        SetSeed(type);

                        if constexpr (std::is_complex_v<Tx>) {
                                using E = std::complex_type_t<Tx>;
                                return static_cast<T>(GetRandom<E>(max.real(), type), GetRandom<E>(max.imag(), type));
                        } else if constexpr (!std::is_floating<T>()) {
                                return static_cast<T>(rand() % static_cast<int>(max+1));
                        } else {

                                double fraction = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
                                return static_cast<T>(fraction * static_cast<double>(max));
                        }
                }

                template <typename T = double, typename Tx = T>
                static inline std::vector<T> GetRandomVector(size_t N, Tx max = 1, long type = E_DEFAULT) {
                        
                        SetSeed(type);
                        
                        std::vector<T> r(N);
                        for (size_t i = 0; i < N; ++i)
                                r[i] = GetRandom<Tx>(max, type);

                        return r;
                }

                static inline unsigned long long GetRandomULL(unsigned long long scale, int type = E_MICROTIMEPID) {

                        SetSeed(type);
                        return (((unsigned long long) (unsigned int)rand() << 32) + (unsigned long long)(unsigned int) rand()) % scale;
                }

                static inline TString GetRandomName(TString prefix, TString suffix = "", int type = E_DEFAULT) {

                        SetSeed(type);
                        TString gRandom = TString::Itoa(GetRandom(100000),10);
                        return prefix + gRandom + suffix;
                }

                ClassDef(Helpers, 1);
        };
} }
