#pragma once

#include <complex>
#include <variant>

// aliases
namespace {
    using complex32 = std::complex<float>;
    using complex64 = std::complex<double>;
    const std::complex<double> i = std::complex<double>(0.0,1.0);
}

namespace std
{
    // Trait to check if a type is a std::complex
    template <typename T>
    struct is_complex : std::false_type {};
    template <typename T>
    struct is_complex<std::complex<T>> : std::true_type {};
    template <typename T>
    inline constexpr bool is_complex_v = is_complex<T>::value;

    // Trait to extract the value type of std::complex
    template <typename T>
    struct complex_type { using type = T; };
    template <typename T>
    struct complex_type<std::complex<T>> { using type = T; };
    template <typename T>
    using complex_type_t = typename complex_type<T>::type;

    // Function template to check if T is a non-floating type
    template <typename T>
    constexpr bool is_floating() { return std::is_floating_point<T>::value; }
    template <typename T>
    struct is_floating_type : std::integral_constant<bool, is_floating<T>()> {};

    //
    // First level complex calculation simplification
    template <typename T>
    bool sort_complex(const std::complex<T> &a, const std::complex<T> &b) {
            return a.real() == b.real() ? a.imag() < b.imag() : a.real() < b.real();
    }

    template <typename Tx, typename Ty>
    bool operator==(const Tx &a, const std::complex<Ty> &b) { 
        using T = typename std::common_type<Tx, Ty>::type;
        return std::complex<T>(a) == std::complex<T>(b);
    }
    template <typename Tx, typename Ty>
    bool operator==(const std::complex<Tx> &a, const Ty &b) { 
        using T = typename std::common_type<Tx, Ty>::type;
        return std::complex<T>(a) == std::complex<T>(b);
    }
    template <typename Tx, typename Ty>
    bool operator!=(const Tx &a, const std::complex<Ty> &b) { 
        using T = typename std::common_type<Tx, Ty>::type;
        return std::complex<T>(a) != std::complex<T>(b);
    }
    template <typename Tx, typename Ty>
    bool operator!=(const std::complex<Tx> &a, const Ty &b) { 
        using T = typename std::common_type<Tx, Ty>::type;
        return std::complex<T>(a) != std::complex<T>(b);
    }

    // Comparison operators: Tx and std::complex<Ty>
    template<typename Tx, typename Ty>
    inline bool operator<(const Tx& a, const std::complex<Ty>& b) {
        using T = typename std::common_type<Tx, Ty>::type;
        T abs_b = std::abs(b);
        T arg_b = std::arg(b);
        T abs_a = static_cast<T>(a);
        return (abs_a < abs_b) || (abs_a == abs_b && arg_b > 0);  // Compare magnitude and angle
    }

    template<typename Tx, typename Ty>
    inline bool operator<=(const Tx& a, const std::complex<Ty>& b) {
        using T = typename std::common_type<Tx, Ty>::type;
        T abs_b = std::abs(b);
        T arg_b = std::arg(b);
        T abs_a = static_cast<T>(a);
        return (abs_a < abs_b) || (abs_a == abs_b && arg_b >= 0);  // Compare magnitude and angle
    }

    template<typename Tx, typename Ty>
    inline bool operator>(const Tx& a, const std::complex<Ty>& b) {
        return !(a <= b);
    }

    template<typename Tx, typename Ty>
    inline bool operator>=(const Tx& a, const std::complex<Ty>& b) {
        return !(a < b);
    }

    // Comparison operators: std::complex<Tx> and Ty
    template<typename Tx, typename Ty>
    inline bool operator<(const std::complex<Tx>& a, const Ty& b) {
        using T = typename std::common_type<Tx, Ty>::type;
        T abs_a = std::abs(a);
        T abs_b = static_cast<T>(b);
        T arg_a = std::arg(a);
        return (abs_a < abs_b) || (abs_a == abs_b && arg_a < 0);  // Compare magnitude and angle
    }

    template<typename Tx, typename Ty>
    inline bool operator<=(const std::complex<Tx>& a, const Ty& b) {
        using T = typename std::common_type<Tx, Ty>::type;
        T abs_a = std::abs(a);
        T abs_b = static_cast<T>(b);
        T arg_a = std::arg(a);
        return (abs_a < abs_b) || (abs_a == abs_b && arg_a <= 0);  // Compare magnitude and angle
    }

    template<typename Tx, typename Ty>
    inline bool operator>(const std::complex<Tx>& a, const Ty& b) {
        return !(a <= b);
    }

    template<typename Tx, typename Ty>
    inline bool operator>=(const std::complex<Tx>& a, const Ty& b) {
        return !(a < b);
    }

    // Comparison operators: const std::complex<Tx>& a and const std::complex<Ty>& b
    template<typename Tx, typename Ty>
    inline bool operator<(const std::complex<Tx>& a, const std::complex<Ty>& b) {
        using T = typename std::common_type<Tx, Ty>::type;
        T abs_a = std::abs(a);
        T abs_b = std::abs(b);
        T arg_a = std::arg(a);
        T arg_b = std::arg(b);
        return (abs_a < abs_b) || (abs_a == abs_b && arg_a < arg_b);  // Compare magnitude and angle
    }

    template<typename Tx, typename Ty>
    inline bool operator<=(const std::complex<Tx>& a, const std::complex<Ty>& b) {
        using T = typename std::common_type<Tx, Ty>::type;
        T abs_a = std::abs(a);
        T abs_b = std::abs(b);
        T arg_a = std::arg(a);
        T arg_b = std::arg(b);
        return (abs_a < abs_b) || (abs_a == abs_b && arg_a <= arg_b);  // Compare magnitude and angle
    }

    template<typename Tx, typename Ty>
    inline bool operator>(const std::complex<Tx>& a, const std::complex<Ty>& b) {
        return !(a <= b);
    }

    template<typename Tx, typename Ty>
    inline bool operator>=(const std::complex<Tx>& a, const std::complex<Ty>& b) {
        return !(a < b);
    }
    template <typename Tx, typename Ty>
    std::complex<typename std::common_type<Tx, Ty>::type> operator* (const Tx &a, const std::complex<Ty> &b) {
        return std::complex<typename std::common_type<Tx, Ty>::type>(a) * std::complex<typename std::common_type<Tx, Ty>::type>(b);
    }

    template <typename Tx, typename Ty>
    std::complex<typename std::common_type<Tx, Ty>::type> operator+ (const Tx &a, const std::complex<Ty> &b) {
        return std::complex<typename std::common_type<Tx, Ty>::type>(a) + std::complex<typename std::common_type<Tx, Ty>::type>(b);
    }

    template <typename Tx, typename Ty>
    std::complex<typename std::common_type<Tx, Ty>::type> operator/ (const Tx &a, const std::complex<Ty> &b) {
        return std::complex<typename std::common_type<Tx, Ty>::type>(a) / std::complex<typename std::common_type<Tx, Ty>::type>(b);
    }

    template <typename Tx, typename Ty>
    std::complex<typename std::common_type<Tx, Ty>::type> operator- (const Tx &a, const std::complex<Ty> &b) {
        return std::complex<typename std::common_type<Tx, Ty>::type>(a) - std::complex<typename std::common_type<Tx, Ty>::type>(b);
    }

    template <typename Tx, typename Ty>
    std::complex<typename std::common_type<Tx, Ty>::type> operator* (const std::complex<Tx> &a, const Ty &b) {
        return std::complex<typename std::common_type<Tx, Ty>::type>(a) * std::complex<typename std::common_type<Tx, Ty>::type>(b);
    }

    template <typename Tx, typename Ty>
    std::complex<typename std::common_type<Tx, Ty>::type> operator+ (const std::complex<Tx> &a, const Ty &b) {
        return std::complex<typename std::common_type<Tx, Ty>::type>(a) + std::complex<typename std::common_type<Tx, Ty>::type>(b);
    }

    template <typename Tx, typename Ty>
    std::complex<typename std::common_type<Tx, Ty>::type> operator/ (const std::complex<Tx> &a, const Ty &b) {
        return std::complex<typename std::common_type<Tx, Ty>::type>(a) / std::complex<typename std::common_type<Tx, Ty>::type>(b);
    }

    template <typename Tx, typename Ty>
    std::complex<typename std::common_type<Tx, Ty>::type> operator- (const std::complex<Tx> &a, const Ty &b) {
        return std::complex<typename std::common_type<Tx, Ty>::type>(a) - std::complex<typename std::common_type<Tx, Ty>::type>(b);
    }

    template <typename Tx, typename Ty>
    std::complex<Tx> operator*=(const std::complex<Tx> &a, const Ty &b) {
        a = a * b;
        return a;
    }

    template <typename Tx, typename Ty>
    std::complex<Tx> operator+=(const std::complex<Tx> &a, const Ty &b) {
        a = a + b;
        return a;
    }

    template <typename Tx, typename Ty>
    std::complex<Tx> operator/=(const std::complex<Tx> &a, const Ty &b) {
        a = a / b;
        return a;
    }

    template <typename Tx, typename Ty>
    std::complex<Tx> operator-=(const std::complex<Tx> &a, const Ty &b) {
        a = a - b;
        return a;
    }

    //
    // Define NumberT<T> as per your implementation
    template <typename T>
    using NumberT = std::variant<T, std::complex<T>>;

    template<typename T>
    std::ostream& operator<<(std::ostream& os, const std::variant<T, std::complex<T>>& v) {
        std::visit([&os](auto&& arg) { os << arg; }, v);
        return os;
    }

    template<typename Ty>
    bool operator==(const std::variant<Ty, std::complex<Ty>>& t, const std::variant<Ty, std::complex<Ty>>& v) { return std::visit([](const auto& lhs, const auto& rhs) { return lhs == rhs; }, t, v); }
    template<typename Tx, typename Ty>
    bool operator==(const Tx& t, const std::variant<Ty, std::complex<Ty>>& v) { return std::visit([&t](const auto& arg) { return arg == t; }, v); }
    template<typename Tx, typename Ty>
    bool operator==(const std::variant<Tx, std::complex<Tx>>& v, const Ty& t) { return std::visit([&t](const auto& arg) { return arg == t; }, v); }

    template<typename Tx, typename Ty>
    bool operator==(const std::complex<Tx>& t, const std::variant<Ty, std::complex<Ty>>& v) { return std::visit([&t](const auto& arg) { return arg == t; }, v); }
    template<typename Tx, typename Ty>
    bool operator==(const std::variant<Tx, std::complex<Tx>>& v, const std::complex<Ty>& t) { return std::visit([&t](const auto& arg) { return arg == t; }, v); }

    template<typename Tx, typename Ty>
    bool operator!=(const std::variant<Ty, std::complex<Ty>>& t, const std::variant<Ty, std::complex<Ty>>& v) { return !(t == v); }
    template<typename Tx, typename Ty>
    bool operator!=(const std::complex<Tx>& t, const std::variant<Ty, std::complex<Ty>>& v) { return !(t == v); }
    template<typename Tx, typename Ty>
    bool operator!=(const Tx& t, const std::variant<Ty, std::complex<Ty>>& v) { return !(t == v); }
    template<typename Tx, typename Ty>
    bool operator!=(const std::variant<Tx, std::complex<Tx>>& v, const Ty& t) { return !(v == t); }

    template <typename T>
    NumberT<T> operator+(const NumberT<T>& lhs, const T& rhs) {
        if (auto* lhs_value = std::get_if<T>(&lhs)) return *lhs_value + rhs; // T + T
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) return *lhs_complex + rhs; // std::complex<T> + T
        
        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator+(const NumberT<T>& lhs, const std::complex<T>& rhs) {

        if (auto* lhs_value = std::get_if<T>(&lhs)) return *lhs_value + rhs; // T + std::complex<T>
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) return *lhs_complex + rhs; // std::complex<T> + std::complex<T>
        
        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator+(const NumberT<T>& lhs, const NumberT<T>& rhs) {

        if (auto* lhs_value = std::get_if<T>(&lhs)) {
            if (auto* rhs_value = std::get_if<T>(&rhs)) return *lhs_value + *rhs_value; // T + T
            if (auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return std::complex<T>(*lhs_value) + *rhs_complex; // T + std::complex<T>
        }
        
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) {
            if (auto* rhs_value = std::get_if<T>(&rhs)) return *lhs_complex + *rhs_value; // std::complex<T> + T
            if (auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return *lhs_complex + *rhs_complex; // std::complex<T> + std::complex<T>
        }

        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator+(const T& lhs, const NumberT<T>& rhs) {
        if (const auto* rhs_value = std::get_if<T>(&rhs)) return lhs + *rhs_value; // T + T
        if (const auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return std::complex<T>(lhs) + *rhs_complex; // T + std::complex<T>
        throw std::runtime_error("Invalid types for operator+");
    }

    template <typename T>
    NumberT<T> operator+(const std::complex<T>& lhs, const NumberT<T>& rhs) {
        if (const auto* rhs_value = std::get_if<T>(&rhs)) return lhs + std::complex<T>(*rhs_value); // std::complex<T> + T
        else if (const auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return lhs + *rhs_complex; // std::complex<T> + std::complex<T>
        throw std::runtime_error("Invalid types for operator+");
    }

    template <typename T>
    NumberT<T>& operator+=(NumberT<T>& lhs, const T& rhs) { lhs = lhs + rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator+=(NumberT<T>& lhs, const std::complex<T>& rhs) { lhs = lhs + rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator+=(const std::complex<T>& rhs, NumberT<T>& lhs) { lhs = lhs + rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator+=(NumberT<T>& lhs, const NumberT<T>& rhs) { lhs = lhs + rhs; return lhs; }



    template <typename T>
    NumberT<T> operator-(const NumberT<T>& lhs, const T& rhs) {
        if (auto* lhs_value = std::get_if<T>(&lhs)) return *lhs_value - rhs; // T - T
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) return *lhs_complex - rhs; // std::complex<T> - T
        
        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator-(const NumberT<T>& lhs, const std::complex<T>& rhs) {

        if (auto* lhs_value = std::get_if<T>(&lhs)) return *lhs_value - rhs; // T - std::complex<T>
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) return *lhs_complex - rhs; // std::complex<T> - std::complex<T>
        
        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator-(const NumberT<T>& lhs, const NumberT<T>& rhs) {

        if (auto* lhs_value = std::get_if<T>(&lhs)) {
            if (auto* rhs_value = std::get_if<T>(&rhs)) return *lhs_value - *rhs_value; // T - T
            if (auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return std::complex<T>(*lhs_value, 0) - *rhs_complex; // T - std::complex<T>
        }
        
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) {
            if (auto* rhs_value = std::get_if<T>(&rhs)) return *lhs_complex - *rhs_value; // std::complex<T> - T
            if (auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return *lhs_complex - *rhs_complex; // std::complex<T> - std::complex<T>
        }

        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator-(const T& lhs, const NumberT<T>& rhs) {
        if (const auto* rhs_value = std::get_if<T>(&rhs)) return lhs - *rhs_value; // T - T
        if (const auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return std::complex<T>(lhs) - *rhs_complex; // std::complex<T> - std::complex<T>
        throw std::runtime_error("Invalid types for operator-");
    }

    template <typename T>
    NumberT<T> operator-(const std::complex<T>& lhs, const NumberT<T>& rhs) {
        if (const auto* rhs_value = std::get_if<T>(&rhs)) return lhs - std::complex<T>(*rhs_value); // T - std::complex<T>
        else if (const auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return lhs - *rhs_complex; // std::complex<T> - std::complex<T>
        throw std::runtime_error("Invalid types for operator-");
    }

    template <typename T>
    NumberT<T>& operator-=(NumberT<T>& lhs, const T& rhs) { lhs = lhs - rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator-=(NumberT<T>& lhs, const std::complex<T>& rhs) { lhs = lhs - rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator-=(const std::complex<T>& rhs, NumberT<T>& lhs) { lhs = lhs - rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator-=(NumberT<T>& lhs, const NumberT<T>& rhs) { lhs = lhs - rhs; return lhs; }




    template <typename T>
    NumberT<T> operator/(const NumberT<T>& lhs, const T& rhs) {
        if (auto* lhs_value = std::get_if<T>(&lhs)) return *lhs_value / rhs; // T / T
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) return *lhs_complex / rhs; // std::complex<T> / T
        
        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator/(const NumberT<T>& lhs, const std::complex<T>& rhs) {

        if (auto* lhs_value = std::get_if<T>(&lhs)) return *lhs_value / rhs; // T / std::complex<T>
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) return *lhs_complex / rhs; // std::complex<T> / std::complex<T>
        
        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator/(const NumberT<T>& lhs, const NumberT<T>& rhs) {

        if (auto* lhs_value = std::get_if<T>(&lhs)) {
            if (auto* rhs_value = std::get_if<T>(&rhs)) return *lhs_value / *rhs_value; // T / T
            if (auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return std::complex<T>(*lhs_value, 0) / *rhs_complex; // T / std::complex<T>
        }
        
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) {
            if (auto* rhs_value = std::get_if<T>(&rhs)) return *lhs_complex / *rhs_value; // std::complex<T> / T
            if (auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return *lhs_complex / *rhs_complex; // std::complex<T> / std::complex<T>
        }

        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator/(const T& lhs, const NumberT<T>& rhs) {
        if (const auto* rhs_value = std::get_if<T>(&rhs)) return lhs / *rhs_value; // T / T
        if (const auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return std::complex<T>(lhs) / *rhs_complex; // std::complex<T> / std::complex<T>
        throw std::runtime_error("Invalid types for operator/");
    }

    template <typename T>
    NumberT<T> operator/(const std::complex<T>& lhs, const NumberT<T>& rhs) {
        if (const auto* rhs_value = std::get_if<T>(&rhs)) return lhs / std::complex<T>(*rhs_value); // T / std::complex<T>
        else if (const auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return lhs / *rhs_complex; // std::complex<T> / std::complex<T>
        throw std::runtime_error("Invalid types for operator/");
    }

    template <typename T>
    NumberT<T>& operator/=(NumberT<T>& lhs, const T& rhs) { lhs = lhs / rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator/=(NumberT<T>& lhs, const std::complex<T>& rhs) { lhs = lhs / rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator/=(const std::complex<T>& rhs, NumberT<T>& lhs) { lhs = lhs / rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator/=(NumberT<T>& lhs, const NumberT<T>& rhs) { lhs = lhs / rhs; return lhs; }




    template <typename T>
    NumberT<T> operator*(const NumberT<T>& lhs, const T& rhs) {
        if (auto* lhs_value = std::get_if<T>(&lhs)) return *lhs_value * rhs; // T * T
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) return *lhs_complex * rhs; // std::complex<T> * T
        
        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator*(const NumberT<T>& lhs, const std::complex<T>& rhs) {

        if (auto* lhs_value = std::get_if<T>(&lhs)) return *lhs_value * rhs; // T * std::complex<T>
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) return *lhs_complex * rhs; // std::complex<T> * std::complex<T>
        
        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator*(const NumberT<T>& lhs, const NumberT<T>& rhs) {

        if (auto* lhs_value = std::get_if<T>(&lhs)) {
            if (auto* rhs_value = std::get_if<T>(&rhs)) return *lhs_value * *rhs_value; // T * T
            if (auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return std::complex<T>(*lhs_value, 0) * *rhs_complex; // T * std::complex<T>
        }
        
        if (auto* lhs_complex = std::get_if<std::complex<T>>(&lhs)) {
            if (auto* rhs_value = std::get_if<T>(&rhs)) return *lhs_complex * *rhs_value; // std::complex<T> * T
            if (auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return *lhs_complex * *rhs_complex; // std::complex<T> * std::complex<T>
        }

        throw std::bad_variant_access(); // Should never reach here with valid types
    }

    template <typename T>
    NumberT<T> operator*(const T& lhs, const NumberT<T>& rhs) {
        if (const auto* rhs_value = std::get_if<T>(&rhs)) return lhs * *rhs_value; // T * T
        if (const auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return std::complex<T>(lhs) * *rhs_complex; // std::complex<T> * std::complex<T>
        throw std::runtime_error("Invalid types for operator*");
    }

    template <typename T>
    NumberT<T> operator*(const std::complex<T>& lhs, const NumberT<T>& rhs) {
        if (const auto* rhs_value = std::get_if<T>(&rhs)) return lhs * std::complex<T>(*rhs_value); // T * std::complex<T>
        else if (const auto* rhs_complex = std::get_if<std::complex<T>>(&rhs)) return lhs * *rhs_complex; // std::complex<T> * std::complex<T>
        throw std::runtime_error("Invalid types for operator*");
    }

    template <typename T>
    NumberT<T>& operator*=(NumberT<T>& lhs, const T& rhs) { lhs = lhs * rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator*=(NumberT<T>& lhs, const std::complex<T>& rhs) { lhs = lhs * rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator*=(const std::complex<T>& rhs, NumberT<T>& lhs) { lhs = lhs * rhs; return lhs; }
    template <typename T>
    NumberT<T>& operator*=(NumberT<T>& lhs, const NumberT<T>& rhs) { lhs = lhs *rhs; return lhs; }

}
