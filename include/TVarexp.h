/**
 * *********************************************
 *
 * \file TVarexp.h
 * \brief Header of the class TVarexp
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 * *********************************************
 *
 * \class TVarexp
 * \brief Organize your 1D,2D-histogram quickly for mass production
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date April 6th, 2016
 *
 * *********************************************
 */

#pragma once

#include <Riostream.h>

#include <TTree.h>
#include <TObject.h>
#include <TString.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TList.h>
#include <TPRegexp.h>
#include <TRegexp.h>
#include <TClass.h>
#include <TSystem.h>
#include <fstream>
#include <TFile.h>
#include <TMath.h>
#include <TFormula.h>
#include <TLine.h>
#include <TF1.h>

#include "TPrint.h"

class TVarexp
{
        private:

                TString varexp;
        public:

                static const bool kWithBinning;
                static const bool kWithoutBinning;
                
                static const bool kSwap;
                
                TVarexp() {};
                TVarexp(const char* newVarexp, bool bWithBinning = TVarexp::kWithBinning) {

                        varexp = newVarexp;
                        varexp.ReplaceAll("::", "#__#");
                        //varexp.ReplaceAll(" ", ""); // Issue if try to tokenize with spaces

                        if(bWithBinning == TVarexp::kWithoutBinning) varexp = this->RemoveBinning();
                };

                virtual ~TVarexp() {};

                static TString StripBranchName(TString);

                int GetN();
                int GetNdimensions();
                static int GetNparameters(TString, TString pattern = "\\[[0-9]*\\]");
                bool HasParameters() {
                        return HasParameters(this->varexp);
                };

                TString Get() {
                        TString varexp0 = this->varexp;
                        varexp0.ReplaceAll("#__#", "::");
                        return varexp0;
                };

                static bool HasParameters(TString, TString pattern = "\\[[0-9]*\\]");
                TString Expand(std::vector<TTree*>, bool = kWithBinning);

                static TString Detokenize(std::vector<TString> vStr, char = ':');
                static TString Detokenize(std::vector<int> vStr, char = ':');
                static TString Detokenize(std::vector<double> vStr, char = ':');
                std::vector<TString> Tokenize(char = ':', bool = kWithBinning);
                std::vector<TString> TokenizeLabel(char = ':');
                std::vector<int> TokenizeNbins(char = ':');
                std::vector<double> TokenizeXmin(char = ':');
                std::vector<double> TokenizeXmax(char = ':');

                TString RemoveBinning();
                TString GetConstantBinningStr(bool = 0);
                TString GetVariableBinningStr(bool = 0);
                TString GetBinningStr(bool b = 0) // IDK if ROOT is able to return a binning str ready to process with for non constant variable..
                {
                        if(HasVariableBinningInformation(varexp)) return GetVariableBinningStr(b); // To be done if required..
                        if(HasConstantBinningInformation(varexp)) return GetConstantBinningStr(b);
                        return "";
                }

                static TString GetTFormulaHistogramName(TH1 *, TString = "", bool = TVarexp::kWithBinning);
                static TString GetTFormulaHistogramBinning(TH1 *, int = 0);
                static TString GetTFormulaHistogramBinning(TH1 *, TString);

                static TString RemoveTFormulaHistogramBinning(TString);
                static bool HasTFormulaBinningInformation(TString);

                static std::vector<std::vector<double>> GetConstantBinning(TString);
                static std::vector<std::vector<double>> GetVariableBinning(TString);
                static std::vector<std::vector<double>> GetBinning(TString varexp)
                {
                        if(HasVariableBinningInformation(varexp)) return GetVariableBinning(varexp);
                        if(HasConstantBinningInformation(varexp)) return GetConstantBinning(varexp);
                        return {};
                }

                std::vector<std::vector<double>> GetConstantBinning()
                {
                        return GetConstantBinning(this->varexp);
                }
                std::vector<std::vector<double>> GetVariableBinning()
                {
                        return GetVariableBinning(this->varexp);
                }
                std::vector<std::vector<double>> GetBinning() {
                        return GetBinning(this->varexp);
                }

                TFormula *GetFormula(TString name = ""); 

                inline static std::vector<double> Eval(TFormula *formula, std::vector<double> params, double x = NAN, double y = NAN, double z = NAN, double t = NAN) { return Eval(formula, params, {}, x, y, z, t); }
                       static std::vector<double> Eval(TFormula *formula, std::vector<double> params, std::vector<double> sigma, double x = NAN, double y = NAN, double z = NAN, double t = NAN);

                static std::vector<TString> ParseName(TString pattern);
                static bool HasBinningInformation(TString);
                static bool HasConstantBinningInformation(TString);
                static bool HasVariableBinningInformation(TString);

                static TString FlipExpression(TString varexp0, char delimiter = ':');
                static std::vector<TString> FlipExpression(std::vector<TString>);

                Long64_t _selectionBuffer = 10000000;    
                std::vector< std::vector<double>> GetSelection(TTree*, TString = "", Long64_t = -1, int = 0, int = 1);

        ClassDef(TVarexp,1);
};
